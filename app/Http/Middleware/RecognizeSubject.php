<?php

namespace App\Http\Middleware;

use App\Domain\Support\Models\User;
use Closure;
use Ensi\LaravelAuditing\Contracts\Principal;
use Ensi\LaravelAuditing\Resolvers\SubjectManager;
use Illuminate\Http\Request;

class RecognizeSubject
{
    /** @var SubjectManager */
    private $manager;

    /** @var array<string, class-string> */
    private $subjects;

    public function __construct(SubjectManager $auditor, array $subjects = [])
    {
        $this->manager = $auditor;
        $this->subjects = $subjects;
    }

    public function handle(Request $request, Closure $next)
    {
        $subject = $this->recognizeUser($request) ?? $this->recognizeAnother($request);

        if ($subject !== null) {
            $this->manager->attach($subject);
        }

        return $next($request);
    }

    private function recognizeUser(Request $request): ?Principal
    {
        $userId = $this->getAuditAttribute($request, 'user_id');
        if ($userId === 0) {
            return null;
        }

        return User::firstOrCreate(['id' => $userId]);
    }

    private function recognizeAnother(Request $request): ?Principal
    {
        foreach ($this->subjects as $key => $className) {
            $id = $this->getAuditAttribute($request, $key);

            if ($id === 0) {
                continue;
            }

            return $className::find($id);
        }

        return null;
    }

    private function getAuditAttribute(Request $request, string $name): int
    {
        return (int)$request->input("audit.{$name}");
    }
}
