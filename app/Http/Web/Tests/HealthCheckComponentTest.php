<?php

use Tests\ComponentTestCase;

uses(ComponentTestCase::class)->group('component');

test('health check')
    ->get('/health')
    ->assertStatus(200)
    ->assertHeader('content-type', 'text/html; charset=UTF-8');
