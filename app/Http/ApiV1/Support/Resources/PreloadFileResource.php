<?php

namespace App\Http\ApiV1\Support\Resources;

use App\Domain\Support\Models\TempFile;

/**
 * @mixin TempFile
 */
class PreloadFileResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'preload_file_id' => $this->id,
            'url' => $this->mapPublicFileToResponse($this->path)?->getUrl(),
        ];
    }
}
