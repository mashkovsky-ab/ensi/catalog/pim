<?php

namespace App\Http\ApiV1\Support\Requests;

use Illuminate\Http\UploadedFile;

class PreloadImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }

    public function image(): UploadedFile
    {
        return $this->file('file');
    }
}
