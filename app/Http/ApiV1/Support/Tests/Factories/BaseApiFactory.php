<?php

namespace App\Http\ApiV1\Support\Tests\Factories;

use Ensi\TestFactories\Factory;
use Ensi\TestFactories\FactoryMissingValue;
use Ensi\TestFactories\PotentiallyMissing;
use Illuminate\Database\Eloquent\Model;

abstract class BaseApiFactory extends Factory
{
    public ?int $id = null;

    public function withId(?int $id = null): self
    {
        return $this->immutableSet('id', $id ?? $this->faker->randomNumber());
    }

    protected function optionalId(): int|PotentiallyMissing
    {
        return $this->notNull($this->id);
    }

    protected function notNull(mixed $value, mixed $default = null): mixed
    {
        if (func_num_args() === 1) {
            $default = new FactoryMissingValue();
        }

        return $this->whenNotNull($value, $value, $default);
    }

    protected function getModelKey(int|string|Model $source): int
    {
        return is_numeric($source) ? (int)$source : $source->getKey();
    }
}
