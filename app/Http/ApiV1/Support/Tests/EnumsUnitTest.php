<?php

use App\Domain\Categories\Models\PropertyType;
use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Http\ApiV1\OpenApiGenerated\Enums\MetricsCategoryEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductFlagEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ProductTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PropertyTypeEnum;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('support', 'unit');

test('PropertyTypeEnum', function ($value) {
    expect(PropertyType::coerce($value))->not->toBeNull();
})->with(PropertyTypeEnum::cases());

test('ProductTypeEnum', function ($value) {
    expect(ProductType::tryFrom($value))->not->toBeNull();
})->with(ProductTypeEnum::cases());

test('ProductStatusEnum', function ($value) {
    expect(ProductStatus::tryFrom($value))->not->toBeNull();
})->with(ProductStatusEnum::cases());

test('MetricsCategoryEnum', function ($value) {
    expect(MetricsCategory::tryFrom($value))->not->toBeNull();
})->with(MetricsCategoryEnum::cases());

test('ProductFlagEnum', function ($value) {
    expect(ProductFlag::tryFrom($value))->not->toBeNull();
})->with(ProductFlagEnum::cases());
