<?php

namespace App\Http\ApiV1\Support\Pagination\Tests;

use App\Http\ApiV1\Support\Pagination\OffsetPageBuilder;
use Tests\UnitTestCase;

uses(UnitTestCase::class, TestsPagination::class)->group('support', 'unit');

beforeEach(function () {
    config()->set('pagination.default_limit', 10);
});

test('OffsetPageBuilder build with positive limit', function () {
    $limit = 10;
    $expectedItems = [1, 2, 3, 4];

    $queryBuilderMock = $this->mockQueryBuilder($expectedItems, skip: 0, limit: $limit);
    $builder = new OffsetPageBuilder($queryBuilderMock, $this->makeRequest($limit));

    $page = $builder->build();

    expect($page->items)->toMatchArray($expectedItems);
    expect($page->pagination)->toMatchArray([
        "offset" => 0,
        "limit" => $limit,
        "total" => 4,
        "type" => "offset",
    ]);
});

test('OffsetPageBuilder build needs another count query if there is more items possibly', function () {
    $limit = 4;
    $total = 12;
    $expectedItems = [1, 2, 3, 4];

    $queryBuilderMock = $this->mockQueryBuilder($expectedItems, $total, 0, $limit);
    $builder = new OffsetPageBuilder($queryBuilderMock, $this->makeRequest($limit));

    $page = $builder->build();

    expect($page->items)->toMatchArray($expectedItems);
    expect($page->pagination)->toMatchArray([
        "offset" => 0,
        "limit" => $limit,
        "total" => $total,
        "type" => "offset",
    ]);
});

test('OffsetPageBuilder build with 0 as limit returns empty array', function () {
    $limit = 0;
    $builder = new OffsetPageBuilder($this->mockQueryBuilder(), $this->makeRequest($limit));

    $page = $builder->build();

    expect($page->items)->toMatchArray([]);
    expect($page->pagination)->toMatchArray([
        "offset" => 0,
        "limit" => $limit,
        "total" => 0,
        "type" => "offset",
    ]);
});

test('OffsetPageBuilder build with negative limit', function () {
    $limit = -1;
    $expectedItems = [1, 2, 3, 4];

    $queryBuilderMock = $this->mockQueryBuilder($expectedItems);
    $builder = new OffsetPageBuilder($queryBuilderMock, $this->makeRequest($limit));

    $page = $builder->build();

    expect($page->items)->toMatchArray($expectedItems);
    expect($page->pagination)->toMatchArray([
        "offset" => 0,
        "limit" => $limit,
        "total" => count($expectedItems),
        "type" => "offset",
    ]);
});

test('OffsetPageBuilder build with negative limit and forbidToBypassPagination=true', function () {
    $limit = -1;
    $builder = new OffsetPageBuilder($this->mockQueryBuilder(), $this->makeRequest($limit));

    $page = $builder->forbidToBypassPagination()->build();

    expect($page->items)->toMatchArray([]);
    expect($page->pagination)->toMatchArray([
        "offset" => 0,
        "limit" => $limit,
        "total" => 0,
        "type" => "offset",
    ]);
});

test('OffsetPageBuilder build with positive limit cannot exceed max limit', function () {
    $limit = 10;
    $maxLimit = 5;
    $expectedItems = [1, 2, 3, 4];

    $queryBuilderMock = $this->mockQueryBuilder($expectedItems, skip: 0, limit: $maxLimit);
    $builder = new OffsetPageBuilder($queryBuilderMock, $this->makeRequest($limit));

    $page = $builder->maxLimit($maxLimit)->build();

    expect($page->items)->toMatchArray($expectedItems);
    expect($page->pagination)->toMatchArray([
        "offset" => 0,
        "limit" => $maxLimit,
        "total" => count($expectedItems),
        "type" => "offset",
    ]);
});
