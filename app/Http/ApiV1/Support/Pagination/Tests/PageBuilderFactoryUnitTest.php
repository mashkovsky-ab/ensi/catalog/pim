<?php

namespace App\Http\ApiV1\Support\Pagination\Tests;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Pagination\CursorPageBuilder;
use App\Http\ApiV1\Support\Pagination\OffsetPageBuilder;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Tests\UnitTestCase;

uses(UnitTestCase::class, TestsPagination::class)->group('support', 'unit');

test('PageBuilderFactory uses default type from config by default', function ($type, $expectedBuilderClassName) {
    config(['pagination.default_type' => $type]);

    $request = $this->makeRequest();
    $pageBuilder = (new PageBuilderFactory())->fromQuery($this->mockQueryBuilder(), $request);

    expect($pageBuilder)->toBeInstanceOf($expectedBuilderClassName);
})->with([
    [PaginationTypeEnum::CURSOR, CursorPageBuilder::class],
    [PaginationTypeEnum::OFFSET, OffsetPageBuilder::class],
]);

test('PageBuilderFactory uses type from request instead of default', function () {
    config(['pagination.default_type' => PaginationTypeEnum::CURSOR]);

    $request = $this->makeRequest(type: PaginationTypeEnum::OFFSET);
    $pageBuilder = (new PageBuilderFactory())->fromQuery($this->mockQueryBuilder(), $request);

    expect($pageBuilder)->toBeInstanceOf(OffsetPageBuilder::class);
});
