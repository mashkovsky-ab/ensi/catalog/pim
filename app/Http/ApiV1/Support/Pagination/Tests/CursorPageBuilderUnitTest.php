<?php

namespace App\Http\ApiV1\Support\Pagination\Tests;

use App\Http\ApiV1\Support\Pagination\CursorPageBuilder;
use Illuminate\Pagination\CursorPaginator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Tests\UnitTestCase;
use UnexpectedValueException;

uses(UnitTestCase::class, TestsPagination::class)->group('support', 'unit');

beforeEach(function () {
    config()->set('pagination.default_limit', 10);
});

test('CursorPageBuilder build with positive limit', function () {
    $limit = 10;
    $expectedItems = [1, 2, 3, 4];

    $queryBuilderMock = $this->mockQueryBuilder();
    $queryBuilderMock->expects('cursorPaginate')->andReturn(new CursorPaginator($expectedItems, $limit));
    $builder = new CursorPageBuilder($queryBuilderMock, $this->makeRequest($limit));

    $page = $builder->build();

    expect($page->items)->toMatchArray($expectedItems);
    expect($page->pagination)->toMatchArray([
        "cursor" => null,
        "limit" => $limit,
        "next_cursor" => null,
        "previous_cursor" => null,
        "type" => "cursor",
    ]);
});

test('CursorPageBuilder build throws BadRequestHttpException if cursor pagination failed with exception', function () {
    $limit = 10;
    $queryBuilderMock = $this->mockQueryBuilder();
    $queryBuilderMock->expects('cursorPaginate')->andThrow(new UnexpectedValueException("test"));
    $builder = new CursorPageBuilder($queryBuilderMock, $this->makeRequest($limit));

    $builder->build();
})->throws(BadRequestHttpException::class);

test('CursorPageBuilder build throws BadRequestHttpException if request cursor can not be decoded', function () {
    $limit = 10;
    $request = $this->makeRequest($limit, 'foo');
    $builder = new CursorPageBuilder($this->mockQueryBuilder(), $request);

    $builder->build();
})->throws(BadRequestHttpException::class);

test('CursorPageBuilder build with 0 as limit returns empty array', function () {
    $limit = 0;
    $builder = new CursorPageBuilder($this->mockQueryBuilder(), $this->makeRequest($limit));

    $page = $builder->build();

    expect($page->items)->toMatchArray([]);
    expect($page->pagination)->toMatchArray([
        "cursor" => null,
        "limit" => $limit,
        "next_cursor" => null,
        "previous_cursor" => null,
        "type" => "cursor",
    ]);
});

test('CursorPageBuilder build with negative limit', function () {
    $limit = -1;
    $expectedItems = [1, 2, 3, 4];

    $queryBuilderMock = $this->mockQueryBuilder($expectedItems);
    $builder = new CursorPageBuilder($queryBuilderMock, $this->makeRequest($limit));

    $page = $builder->build();

    expect($page->items)->toMatchArray($expectedItems);
    expect($page->pagination)->toMatchArray([
        "cursor" => null,
        "limit" => $limit,
        "next_cursor" => null,
        "previous_cursor" => null,
        "type" => "cursor",
    ]);
});

test('CursorPageBuilder build with negative limit and forbidToBypassPagination=true', function () {
    $limit = -1;
    $builder = new CursorPageBuilder($this->mockQueryBuilder(), $this->makeRequest($limit));

    $page = $builder->forbidToBypassPagination()->build();

    expect($page->items)->toMatchArray([]);
    expect($page->pagination)->toMatchArray([
        "cursor" => null,
        "limit" => $limit,
        "next_cursor" => null,
        "previous_cursor" => null,
        "type" => "cursor",
    ]);
});

test('CursorPageBuilder build with positive limit cannot exceed max limit', function () {
    $limit = 10;
    $maxLimit = 5;
    $expectedItems = [1, 2, 3, 4];

    $queryBuilderMock = $this->mockQueryBuilder();
    $queryBuilderMock->expects('cursorPaginate')->andReturn(new CursorPaginator($expectedItems, $maxLimit));
    $builder = new CursorPageBuilder($queryBuilderMock, $this->makeRequest($limit));

    $page = $builder->maxLimit($maxLimit)->build();

    expect($page->items)->toMatchArray($expectedItems);
    expect($page->pagination)->toMatchArray([
        "cursor" => null,
        "limit" => $maxLimit,
        "next_cursor" => null,
        "previous_cursor" => null,
        "type" => "cursor",
    ]);
});
