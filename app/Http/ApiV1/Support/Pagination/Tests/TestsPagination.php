<?php

namespace App\Http\ApiV1\Support\Pagination\Tests;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;

trait TestsPagination
{
    protected function mockQueryBuilder(
        ?array $expectedItems = null,
        ?int $total = null,
        ?int $skip = null,
        ?int $limit = null
    ): MockInterface|Builder {
        $mock = Mockery::mock(Builder::class);

        if ($expectedItems !== null) {
            $mock->expects('get')->andReturn(new Collection($expectedItems));
        }

        if ($total !== null) {
            $mock->expects('count')->andReturn($total);
        }

        if ($skip !== null) {
            $mock->expects('skip')->with($skip)->andReturnSelf();
        }

        if ($limit !== null) {
            $mock->expects('limit')->with($limit)->andReturnSelf();
        }

        if ($skip !== null || $limit !== null) {
            $mock->expects('clone')->andReturnSelf();
        }

        return $mock;
    }

    protected function makeRequest(?int $limit = null, ?string $cursor = null, ?string $type = null): Request
    {
        $request = new Request();

        $pagination = Arr::whereNotNull([
            'limit' => $limit,
            'cursor' => $cursor,
            'type' => $type,
        ]);

        if ($pagination) {
            $request->setMethod('POST');
            $request->request->add(['pagination' => $pagination]);
        }

        return $request;
    }
}
