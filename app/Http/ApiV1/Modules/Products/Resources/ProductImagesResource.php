<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\BaseImage;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin BaseImage
 */
class ProductImagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'sort' => $this->sort,
            'name' => $this->name,

            $this->mergeWhen($this->isInternal(), fn () => $this->getInternal()),
            $this->mergeWhen($this->isExternal(), ['url' => $this->url]),
        ];
    }

    private function getInternal(): array
    {
        $file = $this->mapPublicFileToResponse($this->file);

        return [
            'url' => $file?->getUrl(),
            'file' => $file,
        ];
    }
}
