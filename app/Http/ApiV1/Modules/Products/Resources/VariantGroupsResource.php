<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\VariantGroup;
use App\Http\ApiV1\Modules\Categories\Resources\PropertiesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin VariantGroup
 */
class VariantGroupsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'product_ids' => $this->product_ids,
            'attribute_ids' => $this->attribute_ids,
            'products' => ProductsResource::collection($this->whenLoaded('products')),
            'attributes' => PropertiesResource::collection($this->whenLoaded('properties')),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
