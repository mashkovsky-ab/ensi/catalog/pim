<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Products\Models\PublishedProduct;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin PublishedProduct
 */
class PublishedProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'external_id' => $this->external_id,
            'type' => $this->type,
            'status_id' => $this->status_id,
            'status_comment' => $this->status_comment,
            'allow_publish' => $this->allow_publish,
            'category_id' => $this->category_id,

            'barcode' => $this->barcode,
            'vendor_code' => $this->vendor_code,
            'weight' => $this->weight,
            'weight_gross' => $this->weight_gross,
            'length' => $this->length,
            'height' => $this->height,
            'width' => $this->width,
            'is_adult' => $this->is_adult,

            'main_image' => $this->main_image,
            'images' => $this->whenLoaded(
                'images',
                fn () => ProductImagesResource::collection($this->images)
            ),
        ];
    }
}
