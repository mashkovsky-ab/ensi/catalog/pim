<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Domain\Products\Actions\Variants\CreateVariantGroupAction;
use App\Domain\Products\Actions\Variants\DeleteVariantGroupAction;
use App\Domain\Products\Actions\Variants\PatchVariantGroupAction;
use App\Domain\Products\Actions\Variants\ReplaceVariantGroupAction;
use App\Http\ApiV1\Modules\Products\Queries\VariantGroupsQuery;
use App\Http\ApiV1\Modules\Products\Requests\CreateOrReplaceVariantGroupRequest;
use App\Http\ApiV1\Modules\Products\Requests\PatchVariantGroupRequest;
use App\Http\ApiV1\Modules\Products\Resources\VariantGroupsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class VariantGroupsController
{
    public function create(CreateOrReplaceVariantGroupRequest $request, CreateVariantGroupAction $action): VariantGroupsResource
    {
        return new VariantGroupsResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        CreateOrReplaceVariantGroupRequest $request,
        ReplaceVariantGroupAction $action
    ): VariantGroupsResource {
        return new VariantGroupsResource($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchVariantGroupRequest $request,
        PatchVariantGroupAction $action
    ): VariantGroupsResource {
        return new VariantGroupsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteVariantGroupAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, VariantGroupsQuery $query): VariantGroupsResource
    {
        return new VariantGroupsResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, VariantGroupsQuery $query): AnonymousResourceCollection
    {
        return VariantGroupsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
