<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Domain\Products\Actions\Attributes\PatchProductAttributesAction;
use App\Domain\Products\Actions\Attributes\ReplaceProductAttributesAction;
use App\Domain\Products\Actions\Images\PatchProductImagesAction;
use App\Domain\Products\Actions\Images\ReplaceProductImagesAction;
use App\Domain\Products\Actions\Products\CreateProductAction;
use App\Domain\Products\Actions\Products\DeleteManyProductsAction;
use App\Domain\Products\Actions\Products\DeleteProductAction;
use App\Domain\Products\Actions\Products\PatchProductAction;
use App\Domain\Products\Actions\Products\PreloadImageAction;
use App\Domain\Products\Actions\Products\ReplaceProductAction;
use App\Http\ApiV1\Modules\Products\Queries\ProductDraftsQuery;
use App\Http\ApiV1\Modules\Products\Requests\CreateProductRequest;
use App\Http\ApiV1\Modules\Products\Requests\PatchOrReplaceAttributesRequest;
use App\Http\ApiV1\Modules\Products\Requests\PatchOrReplaceImagesRequest;
use App\Http\ApiV1\Modules\Products\Requests\PatchProductRequest;
use App\Http\ApiV1\Modules\Products\Requests\ReplaceProductRequest;
use App\Http\ApiV1\Modules\Products\Resources\ProductAttributesResource;
use App\Http\ApiV1\Modules\Products\Resources\ProductImagesResource;
use App\Http\ApiV1\Modules\Products\Resources\ProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Requests\PreloadImageRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\PreloadFileResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ProductDraftsQuery $query): AnonymousResourceCollection
    {
        return ProductsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, ProductDraftsQuery $query): ProductsResource
    {
        return new ProductsResource($query->findOrFail($id));
    }

    public function create(CreateProductRequest $request, CreateProductAction $action): ProductsResource
    {
        return new ProductsResource($action->execute($request->validated()));
    }

    public function replace(
        int                   $id,
        ReplaceProductRequest $request,
        ReplaceProductAction  $action
    ): ProductsResource {
        return new ProductsResource($action->execute($id, $request->validated()));
    }

    public function patch(int $id, PatchProductRequest $request, PatchProductAction $action): ProductsResource
    {
        return new ProductsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyProductsAction $action): EmptyResource
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }

    public function replaceAttributes(
        int $id,
        PatchOrReplaceAttributesRequest $request,
        ReplaceProductAttributesAction $action
    ): AnonymousResourceCollection {
        return ProductAttributesResource::collection($action->execute($id, $request->attributeValues()));
    }

    public function patchAttributes(
        int $id,
        PatchOrReplaceAttributesRequest $request,
        PatchProductAttributesAction $action
    ): AnonymousResourceCollection {
        return ProductAttributesResource::collection($action->execute($id, $request->attributeValues()));
    }

    public function preloadImage(PreloadImageRequest $request, PreloadImageAction $action): PreloadFileResource
    {
        return new PreloadFileResource($action->execute($request->image()));
    }

    public function patchImages(
        int $id,
        PatchOrReplaceImagesRequest $request,
        PatchProductImagesAction $action
    ): AnonymousResourceCollection {
        return ProductImagesResource::collection($action->execute($id, $request->images()));
    }

    public function replaceImages(
        int $id,
        PatchOrReplaceImagesRequest $request,
        ReplaceProductImagesAction $action
    ): AnonymousResourceCollection {
        return ProductImagesResource::collection($action->execute($id, $request->images()));
    }
}
