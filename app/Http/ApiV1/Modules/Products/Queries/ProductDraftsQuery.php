<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Products\Models\Product;

class ProductDraftsQuery extends BaseProductsQuery
{
    public function __construct()
    {
        parent::__construct(Product::query());
    }
}
