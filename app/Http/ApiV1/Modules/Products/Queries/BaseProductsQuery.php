<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Classifiers\Enums\ProductStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

abstract class BaseProductsQuery extends QueryBuilder
{
    public function __construct(Builder|Relation $subject)
    {
        parent::__construct($subject);

        $this->allowedSorts(array_merge([
            'id',
            'name',
            'code',
            'created_at',
            'updated_at',
        ], $this->extraSorts()));
        $this->defaultSort('id');

        $this->allowedFilters(array_merge([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('name'),
            AllowedFilter::partial('barcode'),
            AllowedFilter::partial('vendor_code'),
            AllowedFilter::exact('allow_publish'),
            AllowedFilter::exact('is_adult'),
            AllowedFilter::exact('type'),
            AllowedFilter::scope('created_at_lte', 'createdAtTo'),
            AllowedFilter::scope('created_at_gte', 'createdAtFrom'),
            AllowedFilter::scope('updated_at_lte', 'updatedAtTo'),
            AllowedFilter::scope('updated_at_gte', 'updatedAtFrom'),

            AllowedFilter::exact('code'),
            AllowedFilter::exact('status_id')->default(ProductStatus::managedStatuses()),
            AllowedFilter::scope('category_id', 'in_categories'),
            AllowedFilter::exact('brand_id'),
            AllowedFilter::exact('external_id'),
        ], $this->extraFilters()));

        $this->allowedIncludes(array_merge([
            'brand',
            'category',
            'images',
            AllowedInclude::custom('attributes', new PropertyDirectoryValueInclude()),
        ], $this->extraIncludes()));
    }

    protected function extraSorts(): array
    {
        return [];
    }

    protected function extraFilters(): array
    {
        return [];
    }

    protected function extraIncludes(): array
    {
        return [];
    }
}
