<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Products\Models\VariantGroup;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class VariantGroupsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(VariantGroup::query());

        $this->allowedSorts(['id']);
        $this->defaultSort('id');

        $this->allowedFilters(
            AllowedFilter::exact('id'),
        );

        $this->allowedIncludes([
            AllowedInclude::relationship('attributes', 'properties'),
            AllowedInclude::relationship('products.attributes', 'products.properties'),
        ]);
    }
}
