<?php

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Categories\Models\PropertyType;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertSoftDeleted;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('products', 'component');

test(
    'PATCH /api/v1/products/products/{id}/attributes fails validation 400',
    function (bool $dir, bool $file, array $fields) {
        $product = Product::factory()->createOne();

        if (!array_key_exists('property_id', $fields)) {
            $fields['property_id'] = Property::factory()->createOne()->id;
        }
        if ($dir) {
            $fields['directory_value_id'] = PropertyDirectoryValue::factory()->createOne()->id;
        }
        if ($file) {
            $fields['preload_file_id'] = TempFile::create(['path' => '/path/to/image.png'])->id;
        }

        patchJson("/api/v1/products/products/{$product->id}/attributes", ['attributes' => [$fields]])
            ->assertStatus(400);
    }
)->with([
    'value and directory_value_id together' => [true, false, ['value' => 'foo']],
    'value and preload_file_id together' => [false, true, ['value' => 'foo']],
    'directory and file together' => [true, true, ['name' => 'bar']],
    'none are specified' => [false, false, ['name' => 'bar']],
    'missing property' => [false, false, ['property_id' => 100_000, 'value' => 'foo']],
    'missing directory value' => [false, false, ['directory_value_id' => 100_000]],
    'missing preload file' => [false, false, ['preload_file_id' => 100_000]],
]);

test('PATCH /api/v1/products/products/{id}/attributes preload file', function () {
    $product = Product::factory()->createOne();
    $property = Property::factory()
        ->withType(PropertyType::IMAGE())
        ->actual($product->category)
        ->createOne();

    $request = ['attributes' => [
        [
            'property_id' => $property->id,
            'preload_file_id' => TempFile::create(['path' => '/products/01/02/image1.png'])->id,
        ],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonStructure(['data' => [['type', 'name', 'value', 'file']]])
        ->assertJsonPath('data.0.value', '/products/01/02/image1.png');
});

test('PATCH /api/v1/products/products/{id}/attributes external image', function () {
    $image = 'https://ya.ru/images?id=123';
    $product = Product::factory()->createOne();
    $property = Property::factory()
        ->withType(PropertyType::IMAGE())
        ->actual($product->category)
        ->createOne();

    $request = ['attributes' => [
        [
            'property_id' => $property->id,
            'value' => $image,
        ],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.value', $image);
});

test('PATCH /api/v1/products/products/{id}/attributes native value', function (string $type) {
    $propValue = ProductPropertyValue::factory()
        ->withType(PropertyType::coerce($type))
        ->actual()
        ->makeOne();

    $expectedValue = (string)$propValue->getValue();
    $request = ['attributes' => [
        [
            'property_id' => $propValue->property_id,
            'value' => $expectedValue,
        ],
    ]];

    patchJson("/api/v1/products/products/{$propValue->product_id}/attributes", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['property_id', 'value']]])
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.value', $expectedValue);
})->with(PropertyType::asArray());

test('PATCH /api/v1/products/products/{id}/attributes directory value', function () {
    $product = Product::factory()->createOne();
    $property = Property::factory()->directory()->actual($product->category)->createOne();
    $dirValue = PropertyDirectoryValue::factory()->forProperty($property)->createOne();

    $request = ['attributes' => [
        [
            'property_id' => $property->id,
            'directory_value_id' => $dirValue->id,
        ],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['property_id', 'value']]])
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment([
            'property_id' => $property->id,
            'directory_value_id' => $dirValue->id,
            'value' => (string)$dirValue->getValue(),
            'name' => $dirValue->name,
        ]);
});

test('PATCH /api/v1/products/products/{id}/attributes sets multiple values to attribute', function () {
    $product = Product::factory()->createOne();
    $property = Property::factory()
        ->withType(PropertyType::STRING())
        ->actual($product->category)
        ->createOne(['is_multiple' => true]);

    $request = ['attributes' => [
        ['property_id' => $property->id, 'value' => 'foo'],
        ['property_id' => $property->id, 'value' => 'bar'],
    ]];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(2, 'data');
});

test('PATCH /api/v1/products/products/{id}/attributes fails 400', function (array $options, array $values) {
    $product = Product::factory()->createOne();
    $property = Property::factory()
        ->withType(PropertyType::STRING())
        ->actual($product->category)
        ->createOne($options);

    $attributes = [];
    foreach ($values as $value) {
        $value['property_id'] = $property->id;

        if (array_key_exists('directory_value_id', $value)) {
            $value['directory_value_id'] = PropertyDirectoryValue::factory()->forProperty($property)->createOne()->id;
        }

        if (array_key_exists('preload_file_id', $value)) {
            $value['preload_file_id'] = TempFile::create(['path' => '/path/to/image.png'])->id;
        }

        $attributes[] = $value;
    }

    patchJson("/api/v1/products/products/{$product->id}/attributes", ['attributes' => $attributes])
        ->assertStatus(400);
})->with([
    'property does not support directory value' => [
        ['has_directory' => false],
        [['directory_value_id' => 0]],
    ],
    'property does not support native value' => [
        ['has_directory' => true],
        [['value' => 'foo']],
    ],
    'property does not support file' => [
        [],
        [['preload_file_id' => 0]],
    ],
    'property does not support multiple values' => [
        ['is_multiple' => false],
        [['value' => 'foo'], ['value' => 'bar']],
    ],
]);

test('PATCH /api/v1/products/products/{id}/attributes skips not specified attributes', function () {
    $product = Product::factory()->createOne();
    $existing = ProductPropertyValue::factory()->forProduct($product)->actual()->createOne();

    $request = ['attributes' => []];

    patchJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data');

    assertDatabaseHas($existing->getTable(), ['id' => $existing->id, 'deleted_at' => null]);
});

test('PUT /api/v1/products/products/{id}/attributes clears not specified attributes', function () {
    $product = Product::factory()->createOne();
    $existing = ProductPropertyValue::factory()->forProduct($product)->actual()->createOne();

    $request = ['attributes' => []];

    putJson("/api/v1/products/products/{$product->id}/attributes", $request)
        ->assertOk()
        ->assertJsonCount(0, 'data');

    assertSoftDeleted($existing);
});
