<?php

use App\Domain\Products\Models\Product;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Illuminate\Support\Facades\Date;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\travelTo;

uses(ApiV1ComponentTestCase::class)->group('products', 'component');

test('POST /api/v1/products/published:search 200', function () {
    $product = Product::factory()->createOne();

    postJson('/api/v1/products/published:search')
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonStructure(['data' => [['id', 'name', 'code', 'main_image']]])
        ->assertJsonPath('data.0.id', $product->id);
});

test('POST /api/v1/products/published:search with filter success', function () {
    Product::factory()->count(2)->create();
    $product = Product::factory()->createOne(['name' => 'foo']);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/products/published:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
});

test('POST /api/v1/products/published:search multiple ids success', function () {
    $products = Product::factory()->count(3)->create()->take(2);
    $request = [
        'filter' => ['id' => $products->pluck('id')->all()],
    ];

    postJson('/api/v1/products/published:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(2, 'data')
        ->assertJsonPath('data.0.id', $products[0]->id);
});

test('POST /api/v1/products/published:search excludes deleted', function () {
    $product = Product::factory()->createOne();
    Product::factory()->deleted()->createOne();

    postJson('/api/v1/products/published:search', [])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
});

test('POST /api/v1/products/published:search by creating period', function () {
    $product = travelTo(Date::make('2022-05-05 11:25:14'), fn () => Product::factory()->createOne());
    $request = [
        'filter' => [
            'created_at_gte' => '2022-05-05T10:00:00.0Z',
            'created_at_lte' => '2022-05-05T14:00:00.0Z',
        ],
    ];

    postJson('/api/v1/products/published:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
});

test('GET /api/v1/products/published/{id} 200', function () {
    $product = Product::factory()->createOne();

    getJson("/api/v1/products/published/{$product->id}")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'main_image']])
        ->assertJsonPath('data.id', $product->id);
});

test('GET /api/v1/products/published/{id} with includes success', function () {
    $product = Product::factory()->createOne();

    getJson("/api/v1/products/published/{$product->id}?include=images")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'images']]);
});

test('GET /api/v1/products/published/{id} missing 404', function () {
    getJson('/api/v1/products/published/100000')
        ->assertStatus(404);
});

test('GET /api/v1/products/published/{id} deleted 404', function () {
    $product = Product::factory()->deleted()->createOne();

    getJson("/api/v1/products/published/{$product->id}")
        ->assertStatus(404);
});
