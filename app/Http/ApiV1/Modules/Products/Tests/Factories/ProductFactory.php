<?php

namespace App\Http\ApiV1\Modules\Products\Tests\Factories;

use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\TestFactories\FactoryMissingValue;
use Illuminate\Support\Collection;

class ProductFactory extends BaseApiFactory
{
    public ?int $categoryId = null;
    public ?Collection $images = null;
    public ?Collection $attributes = null;

    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug,
            'description' => $this->faker->text,
            'type' => $this->faker->randomElement(ProductType::cases()),
            'status_id' => $this->faker->randomElement(ProductStatus::cases()),
            'status_comment' => $this->faker->optional->sentence,
            'allow_publish' => $this->faker->boolean,

            'external_id' => $this->faker->unique()->numerify('######'),
            'barcode' => $this->faker->ean13,
            'vendor_code' => $this->faker->numerify('###-###-###'),

            'weight' => $this->faker->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->randomFloat(3, 1, 100),
            'length' => $this->faker->randomFloat(2, 1, 1000),
            'height' => $this->faker->randomFloat(2, 1, 1000),
            'width' => $this->faker->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,

            'category_id' => $this->notNull($this->categoryId),
            'images' => $this->executeNested($this->images, new FactoryMissingValue()),
            'attributes' => $this->whenNotNull($this->attributes, $this->attributes?->all()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withCategory(int $categoryId): self
    {
        return $this->immutableSet('categoryId', $categoryId);
    }

    public function withImage(?ProductImageFactory $factory = null): self
    {
        $images = collect($this->images ?? []);
        $images->push($factory ?? ProductImageFactory::new()->external());

        return $this->immutableSet('images', $images);
    }

    public function withAttribute(array $attribute): self
    {
        $attributes = collect($this->attributes ?? null);
        $attributes->push($attribute);

        return $this->immutableSet('attributes', $attributes);
    }

    public function forCreating(): self
    {
        return $this->except(['status_id', 'status_comment']);
    }

    public function onlyRequired(): self
    {
        return $this->only(['name', 'vendor_code', 'type']);
    }
}
