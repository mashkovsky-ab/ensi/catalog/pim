<?php

namespace App\Http\ApiV1\Modules\Products\Tests\Factories;

use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class ProductImageFactory extends BaseApiFactory
{
    public ?string $url = null;
    public ?TempFile $file = null;

    protected function definition(): array
    {
        return [
            'sort' => $this->faker->numberBetween(1, 500),
            'name' => $this->faker->optional->sentence(3),
            'file' => $this->notNull($this->file),
            'url' => $this->notNull($this->url),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function external(): self
    {
        return $this->immutableSet('url', $this->faker->url);
    }

    public function preload(): self
    {
        $fileSuffix = $this->faker->randomNumber();
        $path = "/folder/image_{$fileSuffix}.png";

        return $this->immutableSet('file', TempFile::create(['path' => $path]));
    }
}
