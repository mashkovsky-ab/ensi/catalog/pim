<?php

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('products', 'component');

test('PATCH /api/v1/products/products/{id}/images fails 400', function (bool $image, bool $file, array $fields) {
    $product = Product::factory()->createOne();

    if ($image) {
        $fields['id'] = ProductImage::factory()->owned($product)->createOne()->id;
    }

    if ($file) {
        $fields['preload_file_id'] = TempFile::create(['path' => '/path/to/image.png'])->id;
    }

    patchJson("/api/v1/products/products/{$product->id}/images", ['images' => [$fields]])
        ->assertStatus(400);
})->with([
    'new image without file and url' => [false, false, ['sort' => 10]],
    'new image with file and url' => [false, true, ['url' => 'https://ya.ru/images?p=100']],
    'sets file on existing image' => [true, true, ['name' => 'foo']],
    'sets url on existing images' => [true, false, ['url' => 'https://ya.ru/images?p=100']],
    'missing preload file' => [false, false, ['preload_file_id' => 100_000]],
    'invalid url' => [false, false, ['url' => '100']],
]);

test('PUT /api/v1/products/products/{id}/images adds internal image', function () {
    $product = Product::factory()->createOne();

    $request = [
        'images' => [['preload_file_id' => TempFile::create(['path' => '/path/to/image.png'])->id]],
    ];

    putJson("/api/v1/products/products/{$product->id}/images", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'file', 'sort', 'url']]])
        ->assertJsonCount(1, 'data');

    assertDatabaseHas(ProductImage::tableName(), ['product_id' => $product->id, 'file' => '/path/to/image.png']);
    assertDatabaseHas(PublishedImage::tableName(), ['product_id' => $product->id, 'file' => '/path/to/image.png']);
});

test('PUT /api/v1/products/products/{id}/images adds external image', function () {
    $product = Product::factory()->createOne();

    $request = [
        'images' => [['url' => 'https://images.com/collection/?id=202']],
    ];

    putJson("/api/v1/products/products/{$product->id}/images", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'sort', 'url']]])
        ->assertJsonCount(1, 'data');

    assertDatabaseHas(ProductImage::tableName(), ['product_id' => $product->id, 'url' => 'https://images.com/collection/?id=202']);
    assertDatabaseHas(PublishedImage::tableName(), ['product_id' => $product->id, 'url' => 'https://images.com/collection/?id=202']);
});

test('PATCH /api/v1/products/products/{id}/images updates image', function () {
    $product = Product::factory()->createOne();
    $image = ProductImage::factory()->owned($product)->createOne();

    $request = [
        'images' => [['id' => $image->id, 'name' => 'foo', 'sort' => 50]],
    ];

    patchJson("/api/v1/products/products/{$product->id}/images", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment(['id' => $image->id, 'name' => 'foo', 'sort' => 50]);

    assertDatabaseHas($image->getTable(), ['id' => $image->id, 'name' => 'foo', 'sort' => 50]);
    assertDatabaseHas(PublishedImage::tableName(), ['id' => $image->id, 'name' => 'foo', 'sort' => 50]);
});
