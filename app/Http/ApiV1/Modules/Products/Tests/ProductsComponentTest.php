<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyType;
use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Products\Models\PublishedProduct;
use App\Http\ApiV1\Modules\Products\Tests\Factories\ProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Http\UploadedFile;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('products', 'component');

test('POST /api/v1/products/products success', function () {
    $category = Category::factory()->createOne();
    $request = ProductFactory::new()
        ->withCategory($category->id)
        ->forCreating()
        ->make();

    $id = postJson('/api/v1/products/products', $request)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['id', 'name', 'status_id']])
        ->assertJsonFragment($request)
        ->json('data.id');

    assertDatabaseHas(
        'products',
        ['code' => $request['code'], 'status_id' => ProductStatus::NEW, 'status_comment' => null]
    );
    assertDatabaseHas(
        'published_products',
        ['id' => $id, 'name' => $request['name'], 'code' => $request['code']]
    );
});

test('POST /api/v1/products/products not unique external_id', function () {
    $category = Category::factory()->createOne();
    $existing = Product::factory()->inCategory($category)->createOne();

    $request = ProductFactory::new()
        ->withCategory($category->id)
        ->forCreating()
        ->make(['external_id' => $existing->external_id]);

    postJson('/api/v1/products/products', $request)
        ->assertStatus(400)
        ->assertJsonValidationErrors(['message' => 'external id'], 'errors.0');
});

test('PUT /api/v1/products/products/{id} success', function () {
    $product = Product::factory()->createOne();
    $request = ProductFactory::new()
        ->withCategory($product->category_id)
        ->make(['external_id' => $product->external_id, 'vendor_code' => 'foo']);

    putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonFragment($request);

    assertDatabaseHas($product->getTable(), ['id' => $product->id, 'name' => $request['name']]);
    assertDatabaseHas(PublishedProduct::tableName(), ['id' => $product->id, 'vendor_code' => 'foo']);
});

test('PUT /api/v1/products/products/{id} validation fails', function (array $except, array $extra, string $expected) {
    $product = Product::factory()->createOne();
    $request = ProductFactory::new()
        ->withCategory($product->category_id)
        ->except($except)
        ->make($extra);

    $this->skipNextOpenApiRequestValidation()
        ->putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertStatus(400)
        ->assertJsonValidationErrors(['message' => $expected], 'errors.0');
})->with([
    'without type' => [['type'], [], 'type'],
    'missing category' => [[], ['category_id' => 100_000_000], 'category id'],
    'images is null' => [[], ['images' => null], 'images'],
    'attributes is null' => [[], ['attributes' => null], 'attributes'],
]);

test('PUT /api/v1/products/products/{id} images', function () {
    $product = Product::factory()->createOne();
    $request = ProductFactory::new()
        ->withCategory($product->category_id)
        ->withImage()
        ->make();

    putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data.images');

    assertDatabaseHas(PublishedImage::tableName(), ['product_id' => $product->id]);
});

test('PUT /api/v1/products/products/{id} attributes', function () {
    $product = Product::factory()->createOne();
    $property = Property::factory()
        ->withType(PropertyType::STRING())
        ->actual($product->category)
        ->createOne();

    $request = ProductFactory::new()
        ->withCategory($product->category_id)
        ->withAttribute(['property_id' => $property->id, 'value' => 'foo'])
        ->make();

    putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data.attributes')
        ->assertJsonFragment(['property_id' => $property->id, 'value' => 'foo']);
});

test('PATCH /api/v1/products/products/{id} success', function () {
    $product = Product::factory()->createOne();
    $brand = Brand::factory()->createOne();
    $request = [
        'name' => 'foo',
        'weight' => 220.25,
        'is_adult' => !$product->is_adult,
        'brand_id' => $brand->id,
    ];

    patchJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonFragment($request);

    assertDatabaseHas($product->getTable(), $request);
    assertDatabaseHas(PublishedProduct::tableName(), ['id' => $product->id, 'name' => 'foo']);
});

test('PATCH /api/v1/products/products/{id} attributes and images', function () {
    $product = Product::factory()->createOne();
    $property = Property::factory()
        ->withType(PropertyType::STRING())
        ->actual($product->category)
        ->createOne();

    $request = ProductFactory::new()
        ->withCategory($product->category_id)
        ->withAttribute(['property_id' => $property->id, 'value' => 'foo'])
        ->withImage()
        ->make();

    patchJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data.images')
        ->assertJsonCount(1, 'data.attributes');

    assertDatabaseHas(PublishedImage::tableName(), ['product_id' => $product->id]);
});

test('DELETE /api/v1/products/products/{id} success', function () {
    $product = Product::factory()->createOne(['status_id' => ProductStatus::NEW]);

    deleteJson("/api/v1/products/products/{$product->id}")
        ->assertOk();

    assertDatabaseHas($product->getTable(), ['id' => $product->id, 'status_id' => ProductStatus::DELETED]);
});

test('POST /api/v1/products/products:preload-image success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $file = UploadedFile::fake()->create('123.png', kilobytes: 20);

    $result = postFile('/api/v1/products/products:preload-image', $file)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['preload_file_id', 'url']])
        ->json('data.preload_file_id');

    assertDatabaseHas('temp_files', ['id' => $result]);
});

test('GET /api/v1/products/products success', function () {
    $product = Product::factory()->createOne();

    getJson("/api/v1/products/products/{$product->id}?include=brand,category")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'brand' => ['name'], 'category' => ['name']]]);
});

test('POST /api/v1/products/products required only success', function () {
    $request = ProductFactory::new()->onlyRequired()->make();

    $id = postJson('/api/v1/products/products', $request)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['id', 'name', 'category_id']])
        ->json('data.id');

    assertDatabaseHas(
        'products',
        ['id' => $id, 'status_id' => ProductStatus::NEW, 'allow_publish' => false]
    );
});

test('POST /api/v1/products/products uses default category', function () {
    $category = Category::getOrCreateDefault();
    $request = ProductFactory::new()->onlyRequired()->make();

    postJson('/api/v1/products/products', $request)
        ->assertCreated()
        ->assertJsonPath('data.category_id', $category->id);
});

test('POST /api/v1/products/products with images', function () {
    $request = ProductFactory::new()->withImage()->make();

    $id = postJson('/api/v1/products/products', $request)
        ->assertCreated()
        ->assertJsonCount(1, 'data.images')
        ->json('data.id');

    assertDatabaseHas(PublishedImage::tableName(), ['product_id' => $id]);
});

test('PUT /api/v1/products/products/{id} required only attributes', function () {
    $product = Product::factory()->createOne();
    $request = ProductFactory::new()->onlyRequired()->make();

    putJson("/api/v1/products/products/{$product->id}", $request)
        ->assertOk()
        ->assertJsonFragment($request);
});
