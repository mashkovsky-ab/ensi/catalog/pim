<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PatchProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $productId = $this->route('id');

        $fieldRules = [
            'name' => ['sometimes', 'required', 'string'],
            'category_id' => ['sometimes', 'integer', Rule::exists(Category::class, 'id')],
            'allow_publish' => ['sometimes', 'boolean'],
            'type' => ['sometimes', 'required', new Enum(ProductType::class)],

            'code' => ['sometimes', 'nullable', 'string'],
            'description' => ['sometimes', 'nullable', 'string'],
            'vendor_code' => ['sometimes', 'nullable', 'string'],
            'barcode' => [
                'sometimes',
                'nullable',
                'string',
                Rule::unique(Product::class)->ignore($productId),
            ],
            'external_id' => [
                'sometimes',
                'nullable',
                'string',
                Rule::unique(Product::class)->ignore($productId),
            ],

            'brand_id' => ['sometimes', 'nullable', Rule::exists(Brand::class, 'id')],

            'weight' => ['sometimes', 'nullable', 'numeric'],
            'weight_gross' => ['sometimes', 'nullable', 'numeric'],
            'length' => ['sometimes', 'nullable', 'numeric'],
            'width' => ['sometimes', 'nullable', 'numeric'],
            'height' => ['sometimes', 'nullable', 'numeric'],
            'is_adult' => ['sometimes', 'nullable', 'boolean'],

            'status_id' => ['sometimes', 'required', new Enum(ProductStatus::class)],
            'status_comment' => ['sometimes', 'nullable', 'string'],

            'images' => ['sometimes', 'array'],
            'attributes' => ['sometimes', 'array'],
        ];

        return array_merge(
            $fieldRules,
            PatchOrReplaceImagesRequest::itemRules($productId),
            PatchOrReplaceAttributesRequest::itemRules()
        );
    }
}
