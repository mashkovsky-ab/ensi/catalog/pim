<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class ReplaceProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $productId = $this->route('id');

        $fieldRules = [
            'name' => ['required', 'string'],
            'type' => ['required', new Enum(ProductType::class)],
            'category_id' => ['sometimes', Rule::exists(Category::class, 'id')],
            'allow_publish' => ['sometimes', 'boolean'],

            'code' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'vendor_code' => ['nullable', 'string'],
            'barcode' => [
                'nullable',
                'string',
                Rule::unique(Product::class)->ignore($productId),
            ],
            'external_id' => [
                'nullable',
                'string',
                Rule::unique(Product::class)->ignore($productId),
            ],

            'brand_id' => ['nullable', Rule::exists(Brand::class, 'id')],

            'weight' => ['nullable', 'numeric'],
            'weight_gross' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'is_adult' => ['nullable', 'boolean'],

            'status_id' => ['sometimes', 'required', new Enum(ProductStatus::class)],
            'status_comment' => ['nullable', 'string'],

            'images' => ['sometimes', 'array'],
            'attributes' => ['sometimes', 'array'],
        ];

        return array_merge(
            $fieldRules,
            PatchOrReplaceImagesRequest::itemRules($productId),
            PatchOrReplaceAttributesRequest::itemRules()
        );
    }
}
