<?php

namespace App\Http\ApiV1\Modules\Products\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchVariantGroupRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'product_ids' => ['sometimes', 'array', 'filled'],
            'product_ids.*' => ['integer', 'distinct'],
            'attribute_ids' => ['sometimes', 'array', 'filled'],
            'attribute_ids.*' => ['integer', 'distinct'],
        ];
    }
}
