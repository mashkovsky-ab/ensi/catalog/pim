<?php

namespace App\Http\ApiV1\Modules\Classifiers\Resources;

use App\Domain\Classifiers\Models\ProductStatusSettings;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductStatusSettings
 */
class ProductStatusesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
