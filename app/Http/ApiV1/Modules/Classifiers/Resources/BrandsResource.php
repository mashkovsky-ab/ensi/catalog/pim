<?php

namespace App\Http\ApiV1\Modules\Classifiers\Resources;

use App\Domain\Classifiers\Models\Brand;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Brand
 */
class BrandsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'is_active' => $this->is_active,
            'code' => $this->code,
            'name' => $this->name,
            'description' => $this->description,

            'logo_file' => $this->when(
                $this->logo_file !== null,
                fn () => $this->mapPublicFileToResponse($this->logo_file)
            ),
            'logo_url' => $this->logo_url,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
