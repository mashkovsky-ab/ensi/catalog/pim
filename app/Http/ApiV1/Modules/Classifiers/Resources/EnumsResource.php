<?php

namespace App\Http\ApiV1\Modules\Classifiers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Support\Facades\Lang;

class EnumsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->value,
            'name' => $this->getTitle(),
        ];
    }

    private function getTitle(): string
    {
        $class = get_class($this->resource);
        $key = "enums.{$class}.{$this->resource->value}";

        return Lang::has($key)
            ? __($key)
            : (string)$this->resource->value;
    }
}
