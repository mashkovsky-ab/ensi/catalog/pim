<?php

use App\Domain\Categories\Models\PropertyType;
use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\ProductType;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class)->group('classifiers', 'component');

test('GET /api/v1/classifiers/enums/property-types', function () {
    getJson('/api/v1/classifiers/enums/property-types')
        ->assertOk()
        ->assertJsonCount(count(PropertyType::getInstances()), 'data')
        ->assertJsonFragment(['id' => PropertyType::STRING, 'name' => 'Строка']);
});

test('GET /api/v1/classifiers/enums/product-types', function () {
    getJson('/api/v1/classifiers/enums/product-types')
        ->assertOk()
        ->assertJsonCount(count(ProductType::cases()), 'data')
        ->assertJsonFragment(['id' => ProductType::PIECE->value, 'name' => 'Штучный']);
});

test('GET /api/v1/classifiers/enums/metrics-categories', function () {
    getJson('/api/v1/classifiers/enums/metrics-categories')
        ->assertOk()
        ->assertJsonCount(count(MetricsCategory::cases()), 'data')
        ->assertJsonFragment(['id' => MetricsCategory::ATTRIBUTES, 'name' => 'Атрибуты']);
});
