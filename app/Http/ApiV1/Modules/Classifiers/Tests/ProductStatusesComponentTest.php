<?php

use App\Http\ApiV1\OpenApiGenerated\Enums\ProductStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('classifiers', 'component');

test('PUT /api/v1/classifiers/product-statuses/{id} success', function () {
    $newName = 'Закрыт';
    $request = ['name' => $newName];
    $id = ProductStatusEnum::CLOSED;

    putJson("/api/v1/classifiers/product-statuses/$id", $request)
        ->assertOk()
        ->assertJsonPath('data.name', $newName);

    assertDatabaseHas('product_statuses', ['id' => $id, 'name' => $newName]);
});

test('GET /api/v1/classifiers/product-statuses/{id} success', function () {
    $id = ProductStatusEnum::NEW;

    getJson("/api/v1/classifiers/product-statuses/$id")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name']])
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/classifiers/product-statuses:search success', function () {
    $request = [
        'filter' => ['name' => 'Отклонен'],
    ];

    postJson('/api/v1/classifiers/product-statuses:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', ProductStatusEnum::REJECTED);
});
