<?php

namespace App\Http\ApiV1\Modules\Classifiers\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class BrandFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'code' => $this->faker->optional->slug,
            'name' => $this->faker->company,
            'is_active' => true,
            'description' => $this->faker->optional->sentence,
            'logo_url' => $this->faker->optional->url,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
