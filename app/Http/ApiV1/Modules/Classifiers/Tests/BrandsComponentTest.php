<?php

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Support\Events\FileReleased;
use App\Http\ApiV1\Modules\Classifiers\Tests\Factories\BrandFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('classifiers', 'component');

test('POST /api/v1/classifiers/brands success', function () {
    $request = BrandFactory::new()->make();

    $id = postJson('/api/v1/classifiers/brands', $request)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['id', 'code', 'name', 'logo_url']])
        ->json('data.id');

    assertDatabaseHas(
        (new Brand())->getTable(),
        ['id' => $id, 'name' => $request['name']]
    );
});

test('PUT /api/v1/classifiers/brands/{id} success', function () {
    $brand = Brand::factory()->createOne();
    $request = BrandFactory::new()->make();

    putJson("/api/v1/classifiers/brands/{$brand->id}", $request)
        ->assertOk()
        ->assertJsonFragment(array_filter($request));
});

test('PATCH /api/v1/classifiers/brands/{id} success', function () {
    $brand = Brand::factory()->createOne();
    $request = ['name' => 'foo', 'logo_url' => 'https://images.com?img=12'];

    patchJson("/api/v1/classifiers/brands/{$brand->id}", $request)
        ->assertOk()
        ->assertJsonFragment(array_filter($request))
        ->assertJsonPath('data.logo_file', null);

    assertDatabaseHas(
        $brand->getTable(),
        ['id' => $brand->id, 'name' => 'foo', 'logo_file' => null, 'logo_url' => 'https://images.com?img=12']
    );
});

test('DELETE /api/v1/classifiers/brands/{id} success', function () {
    $brand = Brand::factory()->createOne();

    deleteJson("/api/v1/classifiers/brands/{$brand->id}")
        ->assertOk();

    assertModelMissing($brand);
});

test('DELETE /api/v1/classifiers/brands/{id} with existing product', function () {
    $brand = Brand::factory()->createOne();
    Product::factory()->createOne(['brand_id' => $brand->id]);

    deleteJson("/api/v1/classifiers/brands/{$brand->id}")
        ->assertStatus(400);
});

test('DELETE /api/v1/classifiers/brands/{id} with existing published product', function () {
    $brand = Brand::factory()->createOne();
    $product = Product::factory()->createOne();

    $published = PublishedProduct::findOrFail($product->id);
    $published->brand_id = $brand->id;
    $published->save();

    deleteJson("/api/v1/classifiers/brands/{$brand->id}")
        ->assertStatus(400);
});

test('POST /api/v1/classifiers/brands:search success', function () {
    $brand = Brand::factory()->createOne();

    $request = [
        'filter' => ['code' => $brand->code],
    ];

    postJson('/api/v1/classifiers/brands:search', $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $brand->id);
});

test('GET /api/v1/classifiers/brands/{id} success', function () {
    $brand = Brand::factory()->createOne();

    getJson("/api/v1/classifiers/brands/{$brand->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $brand->id);
});

test('POST /api/v1/classifiers/brands/{id}:upload-image success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $brand = Brand::factory()->withExternalLogo()->createOne();
    $file = UploadedFile::fake()->create('123.png', kilobytes: 20);

    postFile("/api/v1/classifiers/brands/{$brand->id}:upload-image", $file)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'logo_file' => ['url']]])
        ->assertJsonPath('logo_url', null);
});

test('POST /api/v1/classifiers/brands/{id}:delete-image success', function () {
    $brand = Brand::factory()->createOne();
    Event::fake(FileReleased::class);

    postJson("/api/v1/classifiers/brands/{$brand->id}:delete-image")
        ->assertOk()
        ->assertJsonPath('data.logo_file', null);

    Event::assertDispatched(FileReleased::class);
});

test('POST /api/v1/classifiers/brands/{id}:delete-image ignores external logo', function () {
    $brand = Brand::factory()->withExternalLogo()->createOne();

    postJson("/api/v1/classifiers/brands/{$brand->id}:delete-image")
        ->assertOk()
        ->assertJsonPath('data.logo_url', $brand->logo_url);
});
