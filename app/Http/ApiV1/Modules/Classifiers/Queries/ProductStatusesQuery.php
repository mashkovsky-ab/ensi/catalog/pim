<?php

namespace App\Http\ApiV1\Modules\Classifiers\Queries;

use App\Domain\Classifiers\Models\ProductStatusSettings;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductStatusesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductStatusSettings::query());

        $this->allowedSorts(['id', 'name']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('name'),
        ]);
    }
}
