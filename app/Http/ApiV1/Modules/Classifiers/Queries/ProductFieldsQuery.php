<?php

namespace App\Http\ApiV1\Modules\Classifiers\Queries;

use App\Domain\Classifiers\Models\ProductFieldSettings;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductFieldsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductFieldSettings::query());

        $this->allowedSorts(['id', 'name']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('is_required'),
            AllowedFilter::exact('is_moderated'),
            AllowedFilter::exact('metrics_category'),
        ]);
    }
}
