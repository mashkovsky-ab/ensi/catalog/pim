<?php

namespace App\Http\ApiV1\Modules\Classifiers\Queries;

use App\Domain\Classifiers\Models\Brand;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BrandsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Brand::query());

        $this->allowedSorts(['id', 'name', 'code']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('is_active'),
        ]);
    }
}
