<?php

namespace App\Http\ApiV1\Modules\Classifiers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UploadBrandImageRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }
}
