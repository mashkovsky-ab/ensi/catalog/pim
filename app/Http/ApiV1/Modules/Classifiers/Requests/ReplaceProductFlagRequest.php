<?php

namespace App\Http\ApiV1\Modules\Classifiers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReplaceProductFlagRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
        ];
    }
}
