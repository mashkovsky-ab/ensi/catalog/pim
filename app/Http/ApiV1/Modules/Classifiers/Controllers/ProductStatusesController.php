<?php

namespace App\Http\ApiV1\Modules\Classifiers\Controllers;

use App\Domain\Classifiers\Actions\ReplaceProductStatusAction;
use App\Http\ApiV1\Modules\Classifiers\Queries\ProductStatusesQuery;
use App\Http\ApiV1\Modules\Classifiers\Requests\ReplaceProductStatusRequest;
use App\Http\ApiV1\Modules\Classifiers\Resources\ProductStatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductStatusesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ProductStatusesQuery $query): AnonymousResourceCollection
    {
        return ProductStatusesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, ProductStatusesQuery $query): ProductStatusesResource
    {
        return new ProductStatusesResource($query->findOrFail($id));
    }

    public function replace(
        int $id,
        ReplaceProductStatusRequest $request,
        ReplaceProductStatusAction $action
    ): ProductStatusesResource {
        return new ProductStatusesResource($action->execute($id, $request->validated()));
    }
}
