<?php

namespace App\Http\ApiV1\Modules\Classifiers\Controllers;

use App\Domain\Categories\Models\PropertyType;
use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\ProductType;
use App\Http\ApiV1\Modules\Classifiers\Resources\EnumsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EnumsController
{
    public function getPropertyTypes(): AnonymousResourceCollection
    {
        return EnumsResource::collection(array_values(PropertyType::getInstances()));
    }

    public function getProductTypes(): AnonymousResourceCollection
    {
        return EnumsResource::collection(array_values(ProductType::cases()));
    }

    public function getMetricsCategories(): AnonymousResourceCollection
    {
        return EnumsResource::collection(array_values(MetricsCategory::cases()));
    }
}
