<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Domain\Categories\Models\Category;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
            'parent_id' => ['nullable', 'integer', Rule::exists(Category::class, 'id')],
            'is_active' => ['required', 'boolean'],
            'is_inherits_properties' => ['required', 'boolean'],
        ];
    }
}
