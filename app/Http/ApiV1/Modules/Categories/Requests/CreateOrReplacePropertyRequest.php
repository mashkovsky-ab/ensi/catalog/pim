<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Domain\Categories\Models\PropertyType;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use BenSampo\Enum\Rules\EnumValue;

class CreateOrReplacePropertyRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'type' => ['required', new EnumValue(PropertyType::class)],
            'display_name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
            'hint_value' => ['nullable', 'string'],
            'hint_value_name' => ['nullable', 'string'],

            'is_active' => ['boolean'],
            'is_multiple' => ['boolean'],
            'is_filterable' => ['boolean'],
            'is_public' => ['boolean'],
            'is_required' => ['sometimes', 'boolean'],
            'has_directory' => ['boolean'],
            'is_moderated' => ['boolean'],
        ];
    }
}
