<?php

namespace App\Http\ApiV1\Modules\Categories\Requests;

use App\Domain\Categories\Models\Category;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'code' => ['sometimes', 'nullable', 'string'],
            'parent_id' => ['sometimes', 'nullable', 'integer', Rule::exists(Category::class, 'id')],
            'is_active' => ['sometimes', 'boolean'],
            'is_inherits_properties' => ['sometimes', 'boolean'],
        ];
    }
}
