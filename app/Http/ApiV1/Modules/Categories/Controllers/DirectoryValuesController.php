<?php

namespace App\Http\ApiV1\Modules\Categories\Controllers;

use App\Domain\Categories\Actions\DirectoryValues\CreateDirectoryValueAction;
use App\Domain\Categories\Actions\DirectoryValues\DeleteDirectoryValueAction;
use App\Domain\Categories\Actions\DirectoryValues\PatchDirectoryValueAction;
use App\Domain\Categories\Actions\DirectoryValues\PreloadImageAction;
use App\Domain\Categories\Actions\DirectoryValues\ReplaceDirectoryValueAction;
use App\Http\ApiV1\Modules\Categories\Queries\DirectoryValuesQuery;
use App\Http\ApiV1\Modules\Categories\Requests\CreateOrReplaceDirectoryValueRequest;
use App\Http\ApiV1\Modules\Categories\Requests\PatchDirectoryValueRequest;
use App\Http\ApiV1\Modules\Categories\Resources\DirectoryValuesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Requests\PreloadImageRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\PreloadFileResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DirectoryValuesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, DirectoryValuesQuery $query): AnonymousResourceCollection
    {
        return DirectoryValuesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, DirectoryValuesQuery $query): DirectoryValuesResource
    {
        return new DirectoryValuesResource($query->findOrFail($id));
    }

    public function create(
        int $propertyId,
        CreateOrReplaceDirectoryValueRequest $request,
        CreateDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($propertyId, $request->validated()));
    }

    public function replace(
        int $id,
        CreateOrReplaceDirectoryValueRequest $request,
        ReplaceDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchDirectoryValueRequest $request,
        PatchDirectoryValueAction $action
    ): DirectoryValuesResource {
        return new DirectoryValuesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDirectoryValueAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function preloadImage(PreloadImageRequest $request, PreloadImageAction $action): PreloadFileResource
    {
        return new PreloadFileResource($action->execute($request->image()));
    }
}
