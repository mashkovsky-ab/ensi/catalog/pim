<?php

namespace App\Http\ApiV1\Modules\Categories\Tests\Factories;

use App\Domain\Categories\Models\PropertyType;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Illuminate\Support\Facades\Date;

class DirectoryValueFactory extends BaseApiFactory
{
    public ?PropertyType $type = null;

    protected function definition(): array
    {
        $type = $this->type ?? PropertyType::getRandomInstance();

        return [
            'value' => $this->generateValue($type),
            'name' => $this->faker->sentence(3),
            'type' => $type->value,
            'code' => $this->faker->slug(2),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withType(PropertyType $type): self
    {
        return $this->immutableSet('type', $type);
    }

    private function generateValue(PropertyType $type): mixed
    {
        return (string) match ($type->value) {
            PropertyType::STRING => $this->faker->word,
            PropertyType::BOOLEAN => $this->faker->boolean ? '1' : '0',
            PropertyType::COLOR => '#' . dechex($this->faker->numberBetween(0x100000, 0xFFFFFF)),
            PropertyType::DATETIME => Date::make($this->faker->dateTime)->toJSON(),
            PropertyType::DOUBLE => $this->faker->randomFloat(4),
            PropertyType::INTEGER => $this->faker->randomNumber(),
            PropertyType::IMAGE => '/var/www/public/image_' . $this->faker->numberBetween(1, 1000) . '.jpg',
        };
    }
}
