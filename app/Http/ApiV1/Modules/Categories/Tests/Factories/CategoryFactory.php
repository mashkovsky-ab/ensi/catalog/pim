<?php

namespace App\Http\ApiV1\Modules\Categories\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class CategoryFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'code' => $this->faker->slug(2),
            'is_active' => true,
            'parent_id' => null,
            'is_inherits_properties' => true,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
