<?php

use App\Domain\Categories\Events\CategoryActualized;
use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Categories\Models\Property;
use App\Domain\Products\Models\Product;
use App\Http\ApiV1\Modules\Categories\Tests\Factories\CategoryFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Illuminate\Support\Facades\Event;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('categories', 'component');

beforeEach(function () {
    Event::fake([CategoryActualized::class]);
});

test('POST /api/v1/categories/categories success', function () {
    $request = CategoryFactory::new()->make();

    postJson('/api/v1/categories/categories', $request)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => ['id', 'code', 'name', 'parent_id']]);

    Event::assertDispatched(CategoryActualized::class);
    assertDatabaseHas('categories', ['code' => $request['code']]);
});

test('POST /api/v1/categories/categories returns actualized category', function () {
    $parent = Category::factory()->actualized()->createOne(['is_active' => true]);
    $request = CategoryFactory::new()->make(['parent_id' => $parent->id, 'is_active' => true]);

    postJson('/api/v1/categories/categories', $request)
        ->assertCreated()
        ->assertJsonPath('data.is_real_active', true);
});

test('DELETE /api/v1/categories/categories/{id} success', function () {
    $category = Category::factory()->createOne();
    $id = $category->id;

    deleteJson("/api/v1/categories/categories/$id")
        ->assertStatus(200);

    assertModelMissing($category);
});

test('DELETE /api/v1/categories/categories/{id} parent category fails', function () {
    $category = Category::factory()->createTree()->first();
    $id = $category->id;

    deleteJson("/api/v1/categories/categories/$id")
        ->assertStatus(400);

    assertDatabaseHas($category->getTable(), ['id' => $id]);
});

test('DELETE /api/v1/categories/categories/{id} category has products fails', function () {
    $category = Category::factory()->createOne();
    $id = $category->id;
    Product::factory()->inCategory($category)->createOne();

    deleteJson("/api/v1/categories/categories/$id")
        ->assertStatus(400);

    assertDatabaseHas($category->getTable(), ['id' => $id]);
});

test('PATCH /api/v1/categories/categories/{id} success', function () {
    $category = Category::factory()->createOne();
    $id = $category->id;
    $request = CategoryFactory::new()->only(['code'])->make();

    patchJson("/api/v1/categories/categories/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.code', $request['code']);

    Event::assertDispatched(CategoryActualized::class);
    assertDatabaseHas($category->getTable(), ['id' => $id, 'code' => $request['code']]);
});

test('PUT /api/v1/categories/categories/{id} success', function () {
    $category = Category::factory()->createOne();
    $id = $category->id;
    $request = CategoryFactory::new()->make();

    putJson("/api/v1/categories/categories/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.code', $request['code']);

    Event::assertDispatched(CategoryActualized::class);
    assertDatabaseHas($category->getTable(), ['id' => $id, 'code' => $request['code'], 'name' => $request['name']]);
});

test('GET /api/v1/categories/categories/{id} success', function () {
    $category = Category::factory()->createOne();

    getJson("/api/v1/categories/categories/$category->id")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'code', 'name', 'parent_id']]);
});

test('GET /api/v1/categories/categories/{id}?include=properties success', function () {
    $category = Category::factory()->createOne();
    $propertyLink = ActualCategoryProperty::factory()->forCategory($category)->createOne();

    getJson("/api/v1/categories/categories/$category->id?include=properties")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['properties' => [['property_id', 'is_required', 'name']]]])
        ->assertJsonPath('data.properties.0.property_id', $propertyLink->property_id);
});

test('GET /api/v1/categories/categories/{id}?include=hidden_properties success', function () {
    $category = Category::factory()->createOne();
    $propertyLink = CategoryPropertyLink::factory()->forCategory($category)->createOne();

    getJson("/api/v1/categories/categories/$category->id?include=hidden_properties")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['hidden_properties' => [['property_id', 'is_required', 'name']]]])
        ->assertJsonPath('data.hidden_properties.0.property_id', $propertyLink->property_id);
});

test('GET /api/v1/categories/categories/{id}?include=hidden_properties empty success', function () {
    $category = Category::factory()->createOne();
    $propertyLink = ActualCategoryProperty::factory()->forCategory($category)->createOne();

    getJson("/api/v1/categories/categories/$category->id?include=hidden_properties")
        ->assertStatus(200)
        ->assertJsonCount(0, 'data.hidden_properties')
        ->assertJsonCount(1, 'data.properties');
});

test('POST /api/v1/categories/categories:search success', function () {
    $category = Category::factory()->createOne();
    $request = [
        'filter' => ['code' => $category->code],
    ];

    postJson('/api/v1/categories/categories:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.code', $category->code);
});

test('POST /api/v1/categories/categories:search exclude_id success', function () {
    $categories = Category::factory()->count(2)->create();
    $request = [
        'filter' => ['exclude_id' => $categories->first()->id],
    ];

    postJson('/api/v1/categories/categories:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $categories->last()->id);
});

test('POST /api/v1/categories/categories:search is_root success', function () {
    $categories = Category::factory()->createTree();
    $request = [
        'filter' => ['is_root' => true],
    ];

    postJson('/api/v1/categories/categories:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $categories->first()->id);
});

test('POST /api/v1/categories/categories/{id}:bind-properties replace success', function () {
    $link = CategoryPropertyLink::factory()->createOne();
    $id = $link->category_id;

    postJson("/api/v1/categories/categories/$id:bind-properties", ['replace' => true])
        ->assertStatus(200)
        ->assertJsonCount(0, 'data.properties');
});

test('POST /api/v1/categories/categories/{id}:bind-properties common property fails', function () {
    $category = Category::factory()->createOne();
    $property = Property::factory()->common()->createOne();
    $request = ['properties' => [['id' => $property->id, 'is_required' => false]]];

    postJson("/api/v1/categories/categories/$category->id:bind-properties", $request)
        ->assertStatus(400);
});

test('POST /api/v1/categories/categories/{id}:bind-properties patch success', function () {
    $category = Category::factory()->createTree()->first();
    $property = Property::factory()->createOne();
    $request = ['properties' => [['id' => $property->id, 'is_required' => false]]];

    postJson("/api/v1/categories/categories/$category->id:bind-properties", $request)
        ->assertStatus(200)
        ->assertJsonFragment(['property_id' => $property->id, 'is_required' => false]);

    Event::assertDispatched(CategoryActualized::class);
});

test('POST /api/v1/categories/categories:tree in depth success', function () {
    $categories = Category::factory()->createTree(3);
    $deepest = $categories->last();

    postJson("/api/v1/categories/categories:tree")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => [['id', 'name', 'children' => [['id', 'name', 'children' => [['id', 'name']]]]]]])
        ->assertJsonFragment(['id' => $deepest->id, 'name' => $deepest->name]);
});

test('POST /api/v1/categories/categories:tree across success', function () {
    Category::factory()->count(2)->create();

    postJson("/api/v1/categories/categories:tree")
        ->assertStatus(200)
        ->assertJsonCount(2, 'data');
});

test('POST /api/v1/categories/categories:tree only active success', function () {
    $root = Category::factory()->actualized()->createOne();
    $parent = Category::factory()->withParent($root)->createOne(['is_real_active' => false]);
    $child = Category::factory()->withParent($parent)->actualized()->createOne();

    postJson("/api/v1/categories/categories:tree", ['filter' => ['is_active' => true]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonCount(0, 'data.0.children')
        ->assertJsonMissing(['id' => $child->id]);
});
