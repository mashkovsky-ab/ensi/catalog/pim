<?php

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Categories\Models\PropertyType;
use App\Domain\Support\Models\TempFile;
use App\Http\ApiV1\Modules\Categories\Tests\Factories\DirectoryValueFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('categories', 'component');

test('POST /api/v1/categories/properties/{id}:add-directory success', function (PropertyType $type) {
    $property = Property::factory()->createOne(['has_directory' => true, 'type' => $type->value]);
    $propertyId = $property->id;
    $request = DirectoryValueFactory::new()->withType($property->type)->make();

    postJson("/api/v1/categories/properties/$propertyId:add-directory", $request)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => ['id', 'value', 'name', 'code', 'type']])
        ->assertJsonPath('data.value', (string)PropertyValue::make($type, $request['value']));

    assertDatabaseHas('property_directory_values', ['property_id' => $propertyId]);
})->with(PropertyType::getInstances());

test('POST /api/v1/categories/properties/{id}:add-directory invalid value', function () {
    $property = Property::factory()->createOne(['has_directory' => true, 'type' => PropertyType::INTEGER]);
    $propertyId = $property->id;
    $request = DirectoryValueFactory::new()->withType(PropertyType::STRING())->make();

    postJson("/api/v1/categories/properties/$propertyId:add-directory", $request)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', 'ValidationError');

    assertDatabaseMissing('property_directory_values', ['property_id' => $propertyId]);
});

test('POST /api/v1/categories/properties/{id}:add-directory preloaded file', function () {
    $property = Property::factory()->createOne(['has_directory' => true, 'type' => PropertyType::IMAGE]);
    $propertyId = $property->id;
    $tempFile = TempFile::create(['path' => '/123/456/image_1024.png']);
    $request = DirectoryValueFactory::new()
        ->withType($property->type)
        ->except(['value'])
        ->make(['preload_file_id' => $tempFile->id]);

    postJson("/api/v1/categories/properties/$propertyId:add-directory", $request)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => ['id', 'name', 'value', 'file']])
        ->assertJsonPath('data.value', '/123/456/image_1024.png');

    assertModelMissing($tempFile);
});

test('PUT /api/v1/categories/properties/directory/{id} success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::COLOR())->createOne();
    $request = DirectoryValueFactory::new()->withType(PropertyType::COLOR())->make();
    $id = $value->id;

    putJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200);
});

test('PUT /api/v1/categories/properties/directory/{id} not directory property', function () {
    $property = Property::factory()->withType(PropertyType::STRING())->createOne();
    $value = PropertyDirectoryValue::factory()->forProperty($property)->createOne();
    $request = DirectoryValueFactory::new()->withType($value->type)->make();
    $id = $value->id;

    putJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(400);
});

test('PATCH /api/v1/categories/properties/directory/{id} success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::COLOR())->createOne();
    $id = $value->id;
    $request = DirectoryValueFactory::new()->only(['name'])->make();

    patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.name', $request['name']);

    assertDatabaseHas($value->getTable(), ['id' => $id, 'name' => $request['name']]);
});

test('PATCH /api/v1/categories/properties/directory/{id} null value', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::INTEGER())->createOne(['value' => 90]);
    $id = $value->id;
    $request = ['value' => null];

    $this->skipNextOpenApiRequestValidation()
        ->patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(400);

    assertDatabaseHas($value->getTable(), ['id' => $id, 'value' => 90]);
});

test('PATCH /api/v1/categories/properties/directory/{id} preloaded file', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::IMAGE())->createOne();
    $id = $value->id;
    $oldFile = $value->getValue()->native();

    $tempFile = TempFile::create(['path' => '/001/002/image_2048.png']);
    $request = ['preload_file_id' => $tempFile->id];

    patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.value', $tempFile->path);

    assertDatabaseHas('temp_files', ['path' => $oldFile]);
});

test('PATCH /api/v1/categories/properties/directory/{id} to external file', function () {
    $fileUrl = 'https://ya.ru/images/001.jpg';
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::IMAGE())->createOne();
    $id = $value->id;
    $request = ['value' => $fileUrl];

    patchJson("/api/v1/categories/properties/directory/$id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.value', $fileUrl);
});

test('DELETE /api/v1/categories/properties/directory/{id} success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::COLOR())->createOne();
    $id = $value->id;

    deleteJson("/api/v1/categories/properties/directory/$id")
        ->assertStatus(200);

    assertModelMissing($value);
});

test('GET /api/v1/categories/properties/directory/{id} success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::STRING())->createOne();
    $id = $value->id;

    getJson("/api/v1/categories/properties/directory/$id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['code', 'name', 'value', 'type']]);
});

test('POST /api/v1/categories/properties/directory:search success', function () {
    $value = PropertyDirectoryValue::factory()->withType(PropertyType::STRING())->count(2)->create()->last();
    $request = [
        'filter' => ['code' => $value->code],
    ];

    postJson('/api/v1/categories/properties/directory:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonFragment(['code' => $value->code]);
});

test('POST /api/v1/categories/properties/directory:preload-image success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $file = UploadedFile::fake()->create('123.png', kilobytes: 20);

    $result = postFile('/api/v1/categories/properties/directory:preload-image', $file)
        ->assertCreated()
        ->assertJsonStructure(['data' => ['preload_file_id', 'url']])
        ->json('data.preload_file_id');

    assertDatabaseHas('temp_files', ['id' => $result]);
});
