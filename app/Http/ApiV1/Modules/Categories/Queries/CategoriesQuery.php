<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Categories\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\Includes\IncludeInterface;
use Spatie\QueryBuilder\QueryBuilder;

class CategoriesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Category::query());

        $this->allowedSorts(['id', 'name', 'code', 'parent_id']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('parent_id'),
            AllowedFilter::exact('is_real_active'),
            AllowedFilter::callback('is_root', function (Builder $query, $value) {
                if (!is_bool($value)) {
                    return;
                }

                $value === true
                    ? $query->whereIsRoot()
                    : $query->whereNotNull($query->getModel()->getParentIdName());
            }),
            AllowedFilter::callback('exclude_id', function (Builder $query, $ids) {
                return is_array($ids)
                    ? $query->whereNotIn('id', $ids)
                    : $query->where('id', '!=', $ids);
            }),
        ]);

        $this->allowedIncludes([
            'properties',
            AllowedInclude::custom('hidden_properties', new class implements IncludeInterface {
                public function __invoke(Builder $query, string $include)
                {
                    $query->with(['properties', 'boundProperties']);
                }
            }),
        ]);
    }
}
