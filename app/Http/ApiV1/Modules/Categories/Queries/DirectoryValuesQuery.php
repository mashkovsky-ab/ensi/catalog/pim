<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Categories\Models\PropertyDirectoryValue;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DirectoryValuesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(PropertyDirectoryValue::query());

        $this->allowedSorts(['id', 'name', 'code']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('value'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('property_id'),
        ]);
    }
}
