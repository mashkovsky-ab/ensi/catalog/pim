<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Categories\Models\Category;
use Kalnoy\Nestedset\QueryBuilder as NestedSetQueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CategoriesTreeQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Category::query()->select(['id', 'name', 'code', 'parent_id']));

        $this->allowedSorts(['id', 'name', 'code']);
        $this->defaultSort('name');

        $this->allowedFilters([
            AllowedFilter::callback(
                'root_id',
                fn (NestedSetQueryBuilder $query, mixed $value) => $query->whereDescendantOrSelf($value)
            ),
            AllowedFilter::exact('is_active', 'is_real_active'),
        ]);
    }
}
