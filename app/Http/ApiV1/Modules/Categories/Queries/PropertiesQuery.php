<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Categories\Models\Property;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PropertiesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Property::query());

        $this->allowedSorts(['id', 'name', 'code']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('is_filterable'),
            AllowedFilter::exact('is_system'),
            AllowedFilter::exact('is_common'),
            AllowedFilter::exact('is_required'),
            AllowedFilter::exact('is_active')->default(true),
        ]);

        $this->allowedIncludes(['directory']);
    }
}
