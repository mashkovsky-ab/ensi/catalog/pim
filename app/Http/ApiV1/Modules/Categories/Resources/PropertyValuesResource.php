<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Data\PropertyValueHolder;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class PropertyValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        $propertyValue = $this->getHolder()->getValue();

        return [
            'type' => $propertyValue->type()->value,
            'name' => $this->getHolder()->getName(),
            $this->merge($this->normalizeValue($propertyValue)),
        ];
    }

    protected function getHolder(): PropertyValueHolder
    {
        return $this->resource;
    }

    protected function normalizeValue(PropertyValue $propertyValue): array
    {
        if (!$propertyValue->isFile()) {
            return ['value' => (string)$propertyValue];
        }

        return [
            'value' => $propertyValue->native(),
            'file' => $this->mapPublicFileToResponse($propertyValue->native()),
        ];
    }
}
