<?php

namespace App\Providers;

use App\Domain\Categories\Events\CategoryActualized;
use App\Domain\Categories\Events\CategoryInvalidated;
use App\Domain\Categories\Events\PropertyInvalidated;
use App\Domain\Categories\Listeners\ActualizeCategoryListener;
use App\Domain\Categories\Listeners\ActualizePropertyCategoriesListener;
use App\Domain\Categories\Listeners\ActualizeSubCategoriesListener;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Categories\Observers\DirectoryValueObserver;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Classifiers\Observers\BrandObserver;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Listeners\ActualizeProductListener;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Observers\ProductImageObserver;
use App\Domain\Products\Observers\ProductObserver;
use App\Domain\Products\Observers\ProductPropertyValueObserver;
use App\Domain\Support\Events\FileReleased;
use App\Domain\Support\Listeners\ReleaseFileListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        FileReleased::class => [ReleaseFileListener::class],
        CategoryInvalidated::class => [ActualizeCategoryListener::class],
        CategoryActualized::class => [ActualizeSubCategoriesListener::class],
        PropertyInvalidated::class => [ActualizePropertyCategoriesListener::class],
        ProductInvalidated::class => [ActualizeProductListener::class],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        parent::boot();

        PropertyDirectoryValue::observe(DirectoryValueObserver::class);
        ProductPropertyValue::observe(ProductPropertyValueObserver::class);
        ProductImage::observe(ProductImageObserver::class);
        Product::observe(ProductObserver::class);
        Brand::observe(BrandObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
