<?php

namespace App\Providers;

use App\Domain\Products\Publication\ProductPublisher;
use App\Domain\Products\Publication\ProductPublisherFactory;
use App\Http\Middleware\RecognizeSubject;
use App\Support\Extensions\OptionalMacros;
use App\Support\Log\CategoriesLoggerAwareInterface;
use App\Support\Log\DefaultLoggerAwareInterface;
use Carbon\CarbonImmutable;
use Ensi\LaravelAuditing\Resolvers\SubjectManager;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\DateFactory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Optional;
use Illuminate\Support\ServiceProvider;
use ReflectionException;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        DateFactory::useClass(CarbonImmutable::class);

        $this->app->bind(RecognizeSubject::class, function (Application $app) {
            return new RecognizeSubject(
                $app->make(SubjectManager::class),
                $app->make('config')->get('ensi-audit.subjects')
            );
        });

        $this->app->bind(ProductPublisherFactory::class, fn (Application $app) => ProductPublisherFactory::new());
        $this->app->bind(
            ProductPublisher::class,
            fn (Application $app) => $app->make(ProductPublisherFactory::class)->create()
        );

        $this->attachLoggers();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     * @throws ReflectionException
     */
    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        Optional::mixin(new OptionalMacros());
    }

    private function attachLoggers(): void
    {
        $this->app->afterResolving(
            DefaultLoggerAwareInterface::class,
            fn (DefaultLoggerAwareInterface $resolved) => $resolved->setLogger(Log::channel())
        );

        $this->app->afterResolving(
            CategoriesLoggerAwareInterface::class,
            fn (CategoriesLoggerAwareInterface $resolved) => $resolved->setLogger(Log::channel('categories'))
        );
    }
}
