<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{

    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot(): void
    {
        Route::pattern('id', '[0-9]+');

        Route::pattern('entityId', '[0-9]+');
        Route::pattern('resourceId', '[0-9]+');

        $this->routes(function () {
            Route::namespace($this->namespace)
                ->group(app_path('Http/Web/routes.php'));


            Route::prefix('api/v1')
                ->namespace($this->namespace)
                ->middleware('api')
                ->group(app_path('Http/ApiV1/routes.php'));
        });
    }
}
