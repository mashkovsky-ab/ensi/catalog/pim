<?php

namespace App\Support\Actions;

use Spatie\QueueableAction\ActionJob;

class QueueableActionJob extends ActionJob
{
    protected function resolveQueueableProperties($action)
    {
        parent::resolveQueueableProperties($action);

        if (property_exists($action, 'afterCommit')) {
            $this->afterCommit = $action->afterCommit;
        }
    }
}
