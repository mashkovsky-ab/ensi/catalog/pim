<?php

namespace App\Support\Actions\Tests;

use App\Support\Actions\ActionDecorator;
use App\Support\Actions\DecoratesAction;

class DecoratedActionStub
{
    use DecoratesAction;

    private string $suffix = '';
    private string $prefix = '';

    public static function new(): self
    {
        return new self();
    }

    public function execute(string $one, string $two): string
    {
        return "$this->prefix$one&$two$this->suffix";
    }

    public function getSuffix(): string
    {
        return $this->suffix;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function replaceOneOn(string $value): static|ActionDecorator
    {
        return $this->decorateCall(function (callable $chain, string $one, string $two) use ($value) {
            return $chain($value, $two);
        });
    }

    public function replaceTwoOn(string $value): static|ActionDecorator
    {
        return $this->decorateCall(fn (callable $chain, string $one, string $two) => $chain($one, $value));
    }

    public function wrapBrackets(): static|ActionDecorator
    {
        return $this->decorateResult(fn (string $source) => "($source)");
    }

    public function withSuffix(string $value): static|ActionDecorator
    {
        return $this->decorateState(fn () => ['suffix' => $value]);
    }

    public function withPrefix(string $value): static|ActionDecorator
    {
        return $this->decorateState(function (string $one) use ($value) {
            return [
                'prefix' => $one == 'A' ? $value : '/',
            ];
        });
    }
}
