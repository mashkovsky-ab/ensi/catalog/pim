<?php

use App\Support\Actions\Tests\DecoratedActionStub;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('support', 'unit');

test('execute without decorating', function () {
    $action = DecoratedActionStub::new();

    expect($action->execute('A', 'B'))->toBe('A&B');
});

test('decorate', function () {
    $action = DecoratedActionStub::new()->replaceTwoOn('C');

    expect($action->execute('A', 'B'))->toBe('A&C');
});

test('decorate result', function () {
    $action = DecoratedActionStub::new()->wrapBrackets();

    expect($action->execute('A', 'B'))->toBe('(A&B)');
});

test('decorate multi', function () {
    $action = DecoratedActionStub::new()
        ->replaceTwoOn('C')
        ->wrapBrackets()
        ->replaceOneOn('S');

    expect($action->execute('A', 'B'))->toBe('(S&C)');
});

test('decorate field', function () {
    $action = DecoratedActionStub::new()->withSuffix('S');

    expect($action->execute('A', 'B'))->toBe('A&BS');
    expect($action->getSuffix())->toBeEmpty();
});

test('decorated field from parameters', function () {
    $action = DecoratedActionStub::new()->withPrefix('P');

    expect($action->execute('1', 'B'))->toBe('/1&B');
});

test('decorate field multi', function () {
    $action = DecoratedActionStub::new()
        ->withPrefix('P')
        ->wrapBrackets()
        ->withSuffix('S');

    expect($action->execute('A', 'B'))->toBe('(PA&BS)');
    expect($action->getPrefix())->toBeEmpty();
    expect($action->getSuffix())->toBeEmpty();
});
