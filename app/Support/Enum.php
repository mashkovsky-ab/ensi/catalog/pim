<?php

namespace App\Support;

use BenSampo\Enum\Enum as EnumBase;

/**
 * Enum базовый класс для перечислений.
 */
class Enum extends EnumBase
{
    public static function serializeDatabase($value)
    {
        return $value instanceof Enum
            ? $value->value
            : $value;
    }
}
