<?php

namespace App\Support;

use Closure;
use Illuminate\Support\Traits\ForwardsCalls;

/**
 * @template T
 */
class Lazy
{
    use ForwardsCalls;

    protected Closure $factory;
    protected mixed $value = null;

    public function __construct(callable $factory)
    {
        $this->factory = ($factory instanceof Closure) ? $factory : fn () => $factory();
    }

    /**
     * @return T
     */
    public function value(): mixed
    {
        $this->value ??= ($this->factory)();

        return $this->value;
    }

    public function __call(string $name, array $arguments): mixed
    {
        return $this->forwardCallTo($this->value(), $name, $arguments);
    }

    public function __get(string $name)
    {
        return $this->value()->{$name};
    }

    public function __set(string $name, $value): void
    {
        $this->value()->{$name} = $value;
    }

    public function __isset(string $name): bool
    {
        return isset($this->value()->{$name});
    }

    public function __unset(string $name): void
    {
        unset($this->value()->{$name});
    }
}
