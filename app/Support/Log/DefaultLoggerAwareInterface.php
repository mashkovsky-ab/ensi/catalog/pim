<?php

namespace App\Support\Log;

use Psr\Log\LoggerAwareInterface;

/**
 * Интерфейс-маркер для автоматического подключения канала логирования по умолчанию
 * к классам в DI.
 */
interface DefaultLoggerAwareInterface extends LoggerAwareInterface
{
}
