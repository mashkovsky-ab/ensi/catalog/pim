<?php

namespace App\Support\Log;

use Psr\Log\LoggerAwareInterface;

/**
 * Интерфейс-маркер для автоматического подключения канала логирования категорий.
 */
interface CategoriesLoggerAwareInterface extends LoggerAwareInterface
{
}
