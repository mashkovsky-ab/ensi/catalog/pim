<?php

use Illuminate\Support\Facades\Date;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('support', 'unit');

test('is_absolute_url', function (?string $source, bool $expected) {
    expect(is_absolute_url($source))->toBe($expected);
})->with([
    'http' => ['http://ya.ru/images/one.gif', true],
    'https' => ['https://example.com/123.png', true],
    'url with query' => ['https://ya.ru?image=img%205.png', true],
    'internal path' => ['/var/www/images/555.png', false],
    'invalid URL' => ['https://example.com 123', false],
]);

test('coalesce', function (array $values, $expected) {
    expect(coalesce(...$values))->toEqual($expected);
})->with([
    'select first value' => [[1, 2], 1],
    'select first not empty' => [[null, '', ' ', 'val'], 'val'],
    'select object value' => [[Date::make('2021-02-20'), 1], Date::make('2021-02-20')],
]);

test('data_combine_assoc', function (array $keys, array $values, mixed $default, array $expected) {
    expect(data_combine_assoc($keys, $values, $default))->toEqual($expected);
})->with([
    'keys and values' => [['one', 'three'], ['one' => 1, 'two' => 2], null, ['one' => 1, 'three' => null]],
    'only values' => [[], ['one' => 1], null, []],
    'only keys and default' => [['one', 'two'], [], 'value', ['one' => 'value', 'two' => 'value']],
]);
