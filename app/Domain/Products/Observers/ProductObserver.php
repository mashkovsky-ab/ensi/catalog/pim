<?php

namespace App\Domain\Products\Observers;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Support\Concerns\InteractsWithModels;

class ProductObserver
{
    use InteractsWithModels;

    public function created(Product $product): void
    {
        $release = new PublishedProduct();
        $release->id = $product->id;
        $release->status_id = $product->status_id;
        $release->status_comment = $product->status_comment;

        $values = $product->only($product->getFillable());
        foreach ($values as $field => $value) {
            $release->{$field} = $value;
        }

        $this->saveOrThrow($release);
    }
}
