<?php

use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Publication\Data\ProductAggregate;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('load trashed attribute', function () {
    $product = Product::factory()->createOne();
    $attribute = ProductPropertyValue::factory()->forProduct($product)->createOne();
    $attribute->delete();

    $aggregate = ProductAggregate::fromDraft($product);

    expect($aggregate->attributes)->toHaveCount(1);
});

test('load trashed attribute release', function () {
    $product = Product::factory()->createOne();
    $attribute = ProductPropertyValue::factory()
        ->forProduct($product)
        ->published()
        ->createOne();

    $attribute->release->delete();

    $aggregate = ProductAggregate::fromDraft($product);

    expect($aggregate->publishedAttributes())->toHaveCount(1);
});

test('load trashed images', function () {
    $image = ProductImage::factory()->createOne();
    $image->delete();

    $aggregate = ProductAggregate::fromDraft($image->product);

    expect($aggregate->images)->toHaveCount(1);
});

test('save sets the final agreed status from the system', function () {
    $product = Product::factory()->createOne(['status_id' => ProductStatus::AGREED]);
    $aggregate = ProductAggregate::fromDraft($product);

    $aggregate->draft->system_status_id = ProductStatus::REJECTED;
    $aggregate->save();

    expect($aggregate->release->status_id)->toBe(ProductStatus::REJECTED);
});

test('save sets the final not agreed status from the user', function () {
    $product = Product::factory()->createOne(['status_id' => ProductStatus::CLOSED]);
    $aggregate = ProductAggregate::fromDraft($product);

    $aggregate->draft->system_status_id = ProductStatus::REJECTED;
    $aggregate->save();

    expect($aggregate->release->status_id)->toBe(ProductStatus::CLOSED);
});
