<?php

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\PropertyType;
use App\Domain\Products\Publication\Data\Field;
use Illuminate\Support\Fluent;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('publication', 'unit');

test('convert value', function (PropertyType $type, mixed $value, PropertyValue $expected) {
    $model = new Fluent(['foo' => $value]);
    $field = new Field('foo', $type);

    expect($field->value($model))->toEqual($expected);
})->with([
    PropertyType::STRING => [PropertyType::STRING(), 'bar', PropertyValue::STRING('bar')],
    PropertyType::INTEGER => [PropertyType::INTEGER(), 121, PropertyValue::INTEGER(121)],
    PropertyType::DOUBLE => [PropertyType::DOUBLE(), 52.55, PropertyValue::DOUBLE(52.55)],
    PropertyType::BOOLEAN => [PropertyType::BOOLEAN(), true, PropertyValue::BOOLEAN(true)],
    PropertyType::IMAGE => [PropertyType::IMAGE(), '/path/to/image.png', PropertyValue::IMAGE('/path/to/image.png')],
    PropertyType::COLOR => [PropertyType::COLOR(), '#FF00FF', PropertyValue::COLOR('#FF00FF')],
    'null' => [PropertyType::STRING(), null, PropertyValue::STRING()],
]);

test('convert value with custom null', function (PropertyType $type, mixed $value) {
    $model = new Fluent(['foo' => $value]);
    $field = new Field('foo', $type, $value);

    expect($field->value($model)->isNull())->toBeTrue();
})->with([
    PropertyType::STRING => [PropertyType::STRING(), ''],
    PropertyType::INTEGER => [PropertyType::INTEGER(), 0],
    PropertyType::DOUBLE => [PropertyType::DOUBLE(), 0.0],
]);

test('construct dynamic', function () {
    $field = Field::BOOLEAN('foo', false, true);

    expect($field->type)->toEqual(PropertyType::BOOLEAN());
    expect($field->code)->toBe('foo');
    expect($field->nullValue)->toBeFalse();
    expect($field->required)->toBeTrue();
});

test('construct dynamic with named argument', function () {
    $field = Field::STRING('foo', moderated: true);

    expect($field->moderated)->toBeTrue();
});
