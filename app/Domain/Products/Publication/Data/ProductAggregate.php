<?php

namespace App\Domain\Products\Publication\Data;

use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Products\Models\PublishedPropertyValue;
use App\Domain\Support\Concerns\InteractsWithModels;
use Illuminate\Support\Collection;

class ProductAggregate
{
    use InteractsWithModels;

    /** @var Collection<int, ProductPropertyValue> */
    public readonly Collection $attributes;

    /** @var Collection<int, ProductImage> */
    public readonly Collection $images;

    public readonly FlagSet $flags;
    public readonly MetricsEditor $metrics;

    public function __construct(
        public readonly Product $draft,
        public readonly PublishedProduct $release
    ) {
        $this->attributes = $this->loadAttributes();
        $this->images = $this->loadImages();

        $this->flags = new FlagSet($this->draft->id);
        $this->metrics = new MetricsEditor($this->draft->id);
    }

    public static function fromDraft(Product $product): self
    {
        return new self($product, PublishedProduct::findOrFail($product->id));
    }

    private function loadAttributes(): Collection
    {
        return ProductPropertyValue::whereProductId($this->draft->id)
            ->withTrashed()
            ->with('release')
            ->get();
    }

    private function loadImages(): Collection
    {
        return ProductImage::whereProductId($this->draft->id)
            ->withTrashed()
            ->with('release')
            ->get();
    }

    /**
     * @return Collection<int, PublishedPropertyValue>
     */
    public function publishedAttributes(): Collection
    {
        return $this->attributes
            ->where('exists', true)
            ->pluck('release')
            ->filter();
    }

    /**
     * @return Collection<int, PublishedImage>
     */
    public function publishedImages(): Collection
    {
        return $this->images
            ->where('exists', true)
            ->pluck('release')
            ->filter();
    }

    public function finalStatus(): ProductStatus
    {
        return $this->draft->system_status_id ?? $this->draft->status_id;
    }

    public function finalStatusComment(): ?string
    {
        return $this->draft->system_status_id !== null
            ? $this->draft->system_status_comment
            : $this->draft->status_comment;
    }

    public function save(): void
    {
        $this->saveOrThrow($this->draft);

        $this->release->status_id = $this->finalStatus();
        $this->release->status_comment = $this->finalStatusComment();

        $this->saveOrThrow($this->release);

        $this->flags->save();
        $this->metrics->save();
    }
}
