<?php

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\PublishedImage;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\ImagesEditor;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\assertNotSoftDeleted;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('commit publishes image', function () {
    $image = ProductImage::factory()->createOne();

    ImagesEditor::new(moderated: true)->commit(ProductAggregate::fromDraft($image->product));

    assertDatabaseHas(PublishedImage::tableName(), ['id' => $image->id]);
});

test('commit sets main image', function () {
    $image = ProductImage::factory()->createOne();

    $aggregate = ProductAggregate::fromDraft($image->product);
    ImagesEditor::new(moderated: true)->commit($aggregate);

    expect($aggregate->release->main_image)->toBe($image->getActualUrl());
});

test('commit deletes marked image', function () {
    $image = ProductImage::factory()->published()->createOne();
    $image->delete();

    ImagesEditor::new(moderated: true)->commit(ProductAggregate::fromDraft($image->product));

    assertModelMissing($image);
});

test('commit replaces published image attributes', function () {
    $image = ProductImage::factory()->published()->createOne();
    $image->update(['name' => 'foo', 'sort' => 1000]);

    ImagesEditor::new()->commit(ProductAggregate::fromDraft($image->product));

    assertDatabaseHas(
        PublishedImage::tableName(),
        ['id' => $image->id, 'name' => 'foo', 'sort' => 1000]
    );
});

test('update publishes image in unmoderated mode', function () {
    $image = ProductImage::factory()->createOne();

    ImagesEditor::new()->update(ProductAggregate::fromDraft($image->product));

    assertDatabaseHas(PublishedImage::tableName(), ['id' => $image->id]);
});

test('update does nothing in moderated mode', function () {
    $image = ProductImage::factory()->createOne();

    ImagesEditor::new(moderated: true)->update(ProductAggregate::fromDraft($image->product));

    assertDatabaseMissing(PublishedImage::tableName(), ['id' => $image->id]);
});

test('review success', function () {
    $image = ProductImage::factory()->createOne();

    $reviews = ImagesEditor::new(true)->review(ProductAggregate::fromDraft($image->product));

    expect($reviews->where('error', true))->toHaveCount(0);
});

test('review failed', function () {
    $product = Product::factory()->createOne();

    $reviews = ImagesEditor::new(true)->review(ProductAggregate::fromDraft($product));

    expect($reviews->where('error', true))->toHaveCount(1);
});

test('verify success', function () {
    $image = ProductImage::factory()->published()->createOne();

    $reviews = ImagesEditor::new(true)->verify(ProductAggregate::fromDraft($image->product));

    expect($reviews->where('error', true))->toHaveCount(0);
});

test('verify failed', function () {
    $image = ProductImage::factory()->createOne();

    $reviews = ImagesEditor::new(true)->verify(ProductAggregate::fromDraft($image->product));

    expect($reviews->where('error', true))->toHaveCount(1);
});

test('rollback deletes unpublished image', function () {
    $image = ProductImage::factory()->createOne();

    ImagesEditor::new()->rollback(ProductAggregate::fromDraft($image->product));

    assertModelMissing($image);
});

test('rollback restores published image', function () {
    $image = ProductImage::factory()->published()->createOne();
    $image->delete();

    ImagesEditor::new()->rollback(ProductAggregate::fromDraft($image->product));

    assertNotSoftDeleted($image);
});
