<?php

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Property;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Products\Models\PublishedPropertyValue;
use App\Domain\Products\Publication\AttributesEditor;
use App\Domain\Products\Publication\Data\ProductAggregate;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\assertNotSoftDeleted;
use function Pest\Laravel\assertSoftDeleted;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('publication', 'integration');

test('commit deletes marked values', function () {
    $value = ProductPropertyValue::factory()->published()->createOne();
    $value->delete();

    (new AttributesEditor())->commit(ProductAggregate::fromDraft($value->product));

    assertModelMissing($value);
    assertModelMissing($value->release);
});

test('commit publishes new values', function () {
    $value = ProductPropertyValue::factory()->createOne();

    (new AttributesEditor())->commit(ProductAggregate::fromDraft($value->product));

    assertDatabaseHas(PublishedPropertyValue::tableName(), ['id' => $value->id]);
});

test('commit replaces published value', function () {
    $value = ProductPropertyValue::factory()
        ->actual()
        ->published()
        ->createOne();

    $value->update(['name' => 'foo']);

    (new AttributesEditor())->commit(ProductAggregate::fromDraft($value->product));

    assertDatabaseHas(PublishedPropertyValue::tableName(), ['id' => $value->id, 'name' => 'foo']);
});

test('update publishes unmoderated attributes', function () {
    $value = ProductPropertyValue::factory()->actual()->createOne();

    (new AttributesEditor())->update(ProductAggregate::fromDraft($value->product));

    assertDatabaseHas(PublishedPropertyValue::tableName(), ['id' => $value->id]);
});

test('update does not change moderated attributes', function () {
    $property = Property::factory()->moderated()->createOne();
    $value = ProductPropertyValue::factory()
        ->forProperty($property)
        ->actual()
        ->createOne();

    (new AttributesEditor())->update(ProductAggregate::fromDraft($value->product));

    assertDatabaseMissing(PublishedPropertyValue::tableName(), ['id' => $value->id]);
});

test('review failed', function () {
    $property = Property::factory()->moderated()->createOne();
    $product = Product::factory()->createOne();
    ActualCategoryProperty::factory()
        ->forCategory($product->category)
        ->forProperty($property)
        ->required()
        ->createOne();

    $reviews = (new AttributesEditor())->review(ProductAggregate::fromDraft($product));

    expect($reviews->where('error', true))->toHaveCount(1);
});

test('review success', function () {
    $property = Property::factory()->moderated()->createOne();
    $value = ProductPropertyValue::factory()
        ->forProperty($property)
        ->actual(true)
        ->createOne();

    $reviews = (new AttributesEditor())->review(ProductAggregate::fromDraft($value->product));

    expect($reviews->where('error', true))->toHaveCount(0);
});

test('review skips unbound attributes', function () {
    $value = ProductPropertyValue::factory()->createOne();

    $reviews = (new AttributesEditor())->review(ProductAggregate::fromDraft($value->product));

    expect($reviews)->toHaveCount(0);
});

test('verify success', function () {
    $value = ProductPropertyValue::factory()
        ->actual(true)
        ->published()
        ->createOne();

    $reviews = (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    expect($reviews->where('error', true))->toHaveCount(0);
});

test('verify failed', function () {
    $value = ProductPropertyValue::factory()->actual(true)->createOne();

    $reviews = (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    expect($reviews->where('error', true))->toHaveCount(1);
});

test('verify skips unbound attributes', function () {
    $value = ProductPropertyValue::factory()->published()->createOne();

    $results = (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    expect($results)->toHaveCount(0);
});

test('verify marks for deletion missing in the category', function () {
    $value = ProductPropertyValue::factory()->published()->createOne();

    (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    assertSoftDeleted(PublishedPropertyValue::tableName(), ['id' => $value->id]);
});

test('verify restores available in the category', function () {
    $value = ProductPropertyValue::factory()
        ->published()
        ->actual()
        ->createOne();

    $value->release->delete();

    (new AttributesEditor())->verify(ProductAggregate::fromDraft($value->product));

    assertNotSoftDeleted(PublishedPropertyValue::tableName(), ['id' => $value->id]);
});

test('rollback deletes unpublished value', function () {
    $value = ProductPropertyValue::factory()->createOne();

    (new AttributesEditor())->rollback(ProductAggregate::fromDraft($value->product));

    assertModelMissing($value);
});

test('rollback restores published value', function () {
    $value = ProductPropertyValue::factory()->published()->createOne();
    $value->delete();

    (new AttributesEditor())->rollback(ProductAggregate::fromDraft($value->product));

    assertNotSoftDeleted($value);
});
