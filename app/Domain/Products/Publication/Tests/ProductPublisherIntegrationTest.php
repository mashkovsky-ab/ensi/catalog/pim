<?php

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\Data\ReviewResult;
use App\Domain\Products\Publication\ProductPublisher;
use App\Domain\Products\Publication\ProductPublisherFactory;
use function Pest\Laravel\assertDatabaseHas;
use Tests\Cases\PublicationTestCase;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class, PublicationTestCase::class)->group('publication', 'integration');

test('publish success', function () {
    $category = Category::factory()->actualized()->createOne();
    $product = Product::factory()
        ->inCategory($category)
        ->createOne(['status_id' => ProductStatus::AGREED]);

    ProductImage::factory()->owned($product)->createOne();

    ProductPublisherFactory::new()
        ->create()
        ->publish(ProductAggregate::fromDraft($product), true);

    assertDatabaseHas(
        PublishedProduct::tableName(),
        ['id' => $product->id, 'status_id' => ProductStatus::AGREED, 'category_id' => $category->id]
    );
});

test('publish calculates metrics', function () {
    $product = Product::factory()->createOne();
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(reviews: [
            new ReviewResult('name', MetricsCategory::MASTER, true, false),
            new ReviewResult('images', MetricsCategory::CONTENT, false, true),
        ]),
    ]);
    $publisher->publish($aggregate);

    expect($aggregate->metrics->get(MetricsCategory::MASTER)?->errors)->toBe(1);
    expect($aggregate->metrics->get(MetricsCategory::CONTENT)?->filled)->toBe(1);
});

test('publish sets validation result flags', function () {
    $product = Product::factory()->createOne();
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(reviews: [
            new ReviewResult('name', MetricsCategory::MASTER, true, false),
            new ReviewResult('foo', MetricsCategory::ATTRIBUTES, false, true),
        ]),
    ]);
    $publisher->publish($aggregate);

    expect($aggregate->flags->has(ProductFlag::REQUIRED_MASTER_DATA))->toBeTrue();
    expect($aggregate->flags->has(ProductFlag::REQUIRED_ATTRIBUTES))->toBeFalse();
});

test('publish rejects invalid release', function () {
    $product = Product::factory()->createOne(['status_id' => ProductStatus::AGREED]);
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(verifies: [
            new ReviewResult('foo', MetricsCategory::ATTRIBUTES, true, false),
        ]),
    ]);
    $publisher->publish($aggregate);

    assertDatabaseHas(
        PublishedProduct::tableName(),
        ['id' => $product->id, 'status_id' => ProductStatus::REJECTED->value, 'status_comment' => 'Не заполнен обязательный атрибут']
    );
});

test('publish closes release in inactive category', function () {
    $category = Category::factory()->actualized(false)->createOne();
    $product = Product::factory()->inCategory($category)->createOne(['status_id' => ProductStatus::AGREED]);
    $aggregate = ProductAggregate::fromDraft($product);

    (new ProductPublisher([]))->publish($aggregate);

    assertDatabaseHas(
        PublishedProduct::tableName(),
        ['id' => $product->id, 'status_id' => ProductStatus::CLOSED->value, 'status_comment' => 'Категория неактивна']
    );
});

test('publish closes release with inactive brand', function () {
    $brand = Brand::factory()->createOne(['is_active' => false]);
    $category = Category::factory()->actualized()->createOne();
    $product = Product::factory()
        ->inCategory($category)
        ->createOne(['status_id' => ProductStatus::AGREED, 'brand_id' => $brand->id]);

    $aggregate = ProductAggregate::fromDraft($product);

    (new ProductPublisher([]))->publish($aggregate);

    assertDatabaseHas(
        PublishedProduct::tableName(),
        ['id' => $product->id, 'status_id' => ProductStatus::CLOSED->value, 'status_comment' => 'Бренд неактивен']
    );
});

test('publish does not change the status other than agreed', function () {
    $product = Product::factory()->createOne(['status_id' => ProductStatus::CLOSED]);
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(verifies: [
            new ReviewResult('foo', MetricsCategory::ATTRIBUTES, true, false),
        ]),
    ]);
    $publisher->publish($aggregate);

    assertDatabaseHas(
        PublishedProduct::tableName(),
        ['id' => $product->id, 'status_id' => ProductStatus::CLOSED->value, 'status_comment' => null]
    );
});

test('reset calculates metrics and sets validation result flags', function () {
    $product = Product::factory()->createOne();
    $aggregate = ProductAggregate::fromDraft($product);

    $publisher = new ProductPublisher([
        $this->mockEditor(reviews: [
            new ReviewResult('name', MetricsCategory::MASTER, true, false),
        ]),
    ]);
    $publisher->reset($aggregate);

    expect($aggregate->metrics->get(MetricsCategory::MASTER)?->errors)->toBe(1);
    expect($aggregate->flags->has(ProductFlag::REQUIRED_MASTER_DATA))->toBeTrue();
});

test('verify updates release status', function () {
    $category = Category::factory()->actualized()->createOne();
    $product = Product::factory()
        ->inCategory($category)
        ->createOne(['status_id' => ProductStatus::REJECTED]);

    $aggregate = ProductAggregate::fromDraft($product);
    $aggregate->draft->status_id = ProductStatus::AGREED;

    (new ProductPublisher([]))->verify($aggregate);

    assertDatabaseHas(
        PublishedProduct::tableName(),
        ['id' => $product->id, 'status_id' => ProductStatus::AGREED->value]
    );
});

test('verify reviews draft', function () {
    $product = Product::factory()->withFlags(ProductFlag::REQUIRED_MASTER_DATA)->createOne();
    $aggregate = ProductAggregate::fromDraft($product);

    (new ProductPublisher([]))->verify($aggregate);

    expect($aggregate->flags->has(ProductFlag::REQUIRED_MASTER_DATA))->toBeFalse();
});
