<?php

namespace App\Domain\Products\Publication;

use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\Data\ReviewResult;
use Illuminate\Support\Collection;

interface Editor
{
    /**
     * Обновляет в чистовике немодерируемые поля и атрибуты.
     *
     * @param ProductAggregate $aggregate
     * @return void
     */
    public function update(ProductAggregate $aggregate): void;

    /**
     * Фиксирует в черновике все внесенные изменения и переносит их в чистовик.
     *
     * @param ProductAggregate $aggregate
     * @return void
     */
    public function commit(ProductAggregate $aggregate): void;

    /**
     * Выполняет проверку заполнения полей и атрибутов в черновике.
     *
     * @param ProductAggregate $aggregate
     * @return Collection|ReviewResult[]
     */
    public function review(ProductAggregate $aggregate): Collection;

    /**
     * Проверяет текущее состояние чистовика в следствии изменения связанных сущностей.
     *
     * @param ProductAggregate $aggregate
     * @return Collection|ReviewResult[]
     */
    public function verify(ProductAggregate $aggregate): Collection;

    /**
     * Сбрасывает все внесенные в черновик, но неопубликованные изменения.
     *
     * @param ProductAggregate $aggregate
     * @return void
     */
    public function rollback(ProductAggregate $aggregate): void;
}
