<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Models\Property;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class VariantGroup
 * @package App\Models
 *
 * @property int $id
 * @property array|int[] $attribute_ids
 * @property array|int[] $product_ids
 *
 * @property-read Collection|Product[] $products
 * @property-read Collection|Property[] $properties
 */
class VariantGroup extends Model
{
    const FILLABLE = ['product_ids', 'attribute_ids'];

    public $timestamps = false;
    protected $fillable = self::FILLABLE;

    protected $casts = [
        'attribute_ids' => 'array',
        'product_ids' => 'array',
    ];

    public function setAttributeIdsAttribute(array $value): void
    {
        $this->attributes['attribute_ids'] = $this->prepareIds($value);
    }

    public function setProductIdsAttribute(array $value): void
    {
        $this->attributes['product_ids'] = $this->prepareIds($value);
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function properties(): BelongsToMany
    {
        return $this->belongsToMany(Property::class);
    }

    public function addProperty(Property $property): bool
    {
        $groupedProductKeys = [];
        /** @var Product[] $products */
        $products = $this->products()->with(['properties'])->get();
        /** @var Property[] $properties */
        $properties = $this->properties->all();
        $properties[] = $property;
        foreach ($products as $groupedProduct) {
            $groupedProductKeys[] = $groupedProduct->variantKey($properties);
        }
        if (count($groupedProductKeys) != count(array_unique($groupedProductKeys))) {
            return false;
        }

        $this->properties()->attach($property->id);

        return true;
    }

    public function addProduct(Product $product): bool
    {
        if ($product->variant_group_id) {
            return false;
        }
        $groupedProductKeys = [];
        /** @var Product[] $products */
        $products = $this->products()->with(['properties'])->get();
        /** @var Property[] $properties */
        $properties = $this->properties->all();
        foreach ($products as $groupedProduct) {
            $groupedProductKeys[] = $groupedProduct->variantKey($properties);
        }
        $currentProductKey = $product->variantKey($properties);
        if (in_array($currentProductKey, $groupedProductKeys)) {
            return false;
        }

        $product->variant_group_id = $this->id;

        return $product->save();
    }

    /**
     * @param callable $callback - function (Property $property, ?ProductPropertyValue $value)
     * @return array [product_id => callback_result]
     */
    public function variants($callback)
    {
        $result = [];
        /** @var Product[] $products */
        $products = $this->products()->with(['properties'])->get();
        /** @var Property[] $properties */
        $properties = $this->properties->all();
        foreach ($products as $groupedProduct) {
            $result[$groupedProduct->id] = $groupedProduct->variantInfo($callback, $properties);
        }

        return $result;
    }

    private function prepareIds(array $source): string
    {
        $values = array_map('intval', $source);
        sort($values, SORT_NUMERIC);

        return json_encode($values);
    }
}
