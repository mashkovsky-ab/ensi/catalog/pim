<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Models\CategoryPropertyView;
use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\ProductFlagSettings;
use App\Domain\Products\Models\Tests\Factories\ProductFactory;
use App\Domain\Support\Models\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Collection;

/**
 * Класс-модель для сущности "Товар".
 *
 * @property bool $allow_publish Признак активности для витрины
 * @property string|null $external_id Идентификатор товара во внешней системе
 * @property int $category_id Идентификатор категории
 * @property ProductType $type Тип товара (весовой, штучный, ...) из перечисления ProductTypeEnum
 * @property ProductStatus $status_id Статус товара из перечисления ProductStatus
 * @property string|null $status_comment Комментарий к статусу
 * @property string|null $barcode Штрихкод (EAN)
 * @property string|null $vendor_code Артикул
 * @property string $name Название
 * @property string $code ЧПУ код
 * @property string|null $description Описание
 * @property float|null $weight Вес нетто (кг)
 * @property float|null $weight_gross Вес брутто (кг)
 * @property float|null $width Ширина (мм)
 * @property float|null $height Высота (мм)
 * @property float|null $length Длина (мм)
 * @property int|null $brand_id Идентификатор бренда
 * @property bool $is_adult Товар 18+
 *
 * @property ProductStatus|null $system_status_id Статус товара определенный системой
 * @property string|null $system_status_comment Комментарий к системному статусу
 *
 * @property-read Collection|ProductPropertyValue[] $properties Значения свойств
 * @property-read Collection|ProductImage[] $images Изображения товара
 * @property-read Collection|ProductFlagSettings[] $flags Установленные признаки товара
 * @property-read Collection|ProductMetrics[] $metrics Метрики валидации
 * @property-read Collection|CategoryPropertyView[] $availableProperties Доступные для товара атрибуты
 *
 * @method static Builder|self inCategories(array|int $categoryIds)
 */
class Product extends Model implements Auditable
{
    use Sluggable;
    use SupportsAudit;
    use ExpandsProduct;

    /** Заполняемые поля модели */
    const FILLABLE = [
        'category_id', 'barcode', 'vendor_code', 'external_id',
        'name', 'code', 'description', 'type', 'status_id', 'status_comment',
        'weight', 'width', 'height', 'length', 'weight_gross',
        'brand_id', 'is_adult', 'allow_publish',
    ];

    protected $fillable = self::FILLABLE;

    protected $casts = [
        'weight' => 'float',
        'weight_gross' => 'float',
        'width' => 'float',
        'height' => 'float',
        'length' => 'float',
        'category_id' => 'int',
        'brand_id' => 'int',
        'status_id' => ProductStatus::class,
        'type' => ProductType::class,
        'is_adult' => 'bool',
        'allow_publish' => 'bool',
        'system_status_id' => ProductStatus::class,
    ];

    protected $attributes = [
        'is_adult' => false,
        'status_id' => ProductStatus::NEW,
        'allow_publish' => false,
    ];

    public function sluggable(): array
    {
        return [
            'code' => [
                'source' => 'name',
            ],
        ];
    }

    public function properties(): HasMany
    {
        return $this->hasMany(ProductPropertyValue::class, 'product_id');
    }

    public function images(): HasMany
    {
        return $this->hasMany(ProductImage::class)
            ->orderBy('product_id')
            ->orderBy('sort');
    }

    public function flags(): HasManyThrough
    {
        return $this->hasManyThrough(
            ProductFlagSettings::class,
            ProductFlagValue::class,
            'product_id',
            'id',
            'id',
            'value'
        );
    }

    public function metrics(): HasMany
    {
        return $this->hasMany(ProductMetrics::class);
    }

    public function availableProperties(): HasMany
    {
        return $this->hasMany(CategoryPropertyView::class, 'category_id', 'category_id');
    }

    protected static function boot()
    {
        parent::boot();

        self::saving(function (self $product) {
            if ($product->status_id === ProductStatus::AGREED) {
                return;
            }

            $product->system_status_id = null;
            $product->system_status_comment = null;
        });
    }

    public static function factory(): ProductFactory
    {
        return ProductFactory::new();
    }
}
