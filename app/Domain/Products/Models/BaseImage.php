<?php

namespace App\Domain\Products\Models;

use App\Domain\Support\Models\Model;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;

/**
 * @property int $id Идентификатор картинки
 * @property int $product_id Идентификатор товара
 * @property int $sort Порядок сортировки
 * @property string|null $file Файл картинки
 * @property string|null $url URL внешнего файла
 * @property string|null $name Описание картинки
 */
abstract class BaseImage extends Model
{
    protected $casts = [
        'product_id' => 'int',
        'sort' => 'int',
    ];

    public function isExternal(): bool
    {
        return filled($this->url);
    }

    public function isInternal(): bool
    {
        return filled($this->file);
    }

    public function getActualUrl(): string
    {
        $url = $this->isInternal()
            ? EnsiFile::public($this->file)?->getUrl()
            : $this->url;

        return $url ?? '';
    }
}
