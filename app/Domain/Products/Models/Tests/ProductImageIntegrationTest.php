<?php

use App\Domain\Products\Models\ProductImage;
use App\Domain\Support\Events\FileReleased;
use Illuminate\Support\Facades\Event;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

beforeEach(function () {
    Event::fake(FileReleased::class);
});

test('deleting image with file raises FileReleased', function () {
    $image = ProductImage::factory()->createOne();

    $image->delete();

    Event::assertDispatched(FileReleased::class);
});

test('deleting image with external link not raises FileReleased', function () {
    $image = ProductImage::factory()->external()->createOne();

    $image->delete();

    Event::assertNotDispatched(FileReleased::class);
});
