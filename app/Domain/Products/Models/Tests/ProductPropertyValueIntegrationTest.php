<?php

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Categories\Models\PropertyType;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Events\FileReleased;
use Illuminate\Support\Facades\Event;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

beforeEach(function () {
    Event::fake(FileReleased::class);
});

test('make from directory value', function () {
    $dirValue = PropertyDirectoryValue::factory()->createOne();
    $product = Product::factory()->createOne();

    $result = ProductPropertyValue::make($product, $dirValue->property, $dirValue);

    expect($result->getValue())->toEqual($dirValue->getValue());
    expect($result->directoryValue->is($dirValue))->toBeTrue();
});

test('make from raw value', function () {
    $product = Product::factory()->createOne();
    $property = Property::factory()->withType(PropertyType::DATETIME())->createOne();
    $timestamp = now();

    $result = ProductPropertyValue::make($product, $property, $timestamp);

    expect($result->getValue())->toEqual(PropertyValue::DATETIME($timestamp));
});

test('set own value', function () {
    $value = PropertyValue::DOUBLE(125.5);

    $testing = ProductPropertyValue::factory()->makeOne()->setOwnValue($value);

    expect($testing->getValue())->toEqual($value);
    expect($testing->isDirectory())->toBeFalse();
});

test('set directory value', function () {
    $dirValue = PropertyDirectoryValue::factory()->createOne();

    $testing = ProductPropertyValue::factory()->makeOne()->setDirectoryValue($dirValue);

    expect($testing->getValue())->toEqual($dirValue->getValue());
    expect($testing->isDirectory())->toBeTrue();
});

test('deleting internal file raises FileReleased', function () {
    $testing = ProductPropertyValue::factory()
        ->withType(PropertyType::IMAGE())
        ->createOne();

    $testing->delete();

    Event::assertDispatched(FileReleased::class);
});

test('deleting external file link not raises FileReleased', function () {
    $testing = ProductPropertyValue::factory()
        ->withType(PropertyType::IMAGE())
        ->createOne(['value' => 'https://images.com/collection/one.jpg']);

    $testing->delete();

    Event::assertNotDispatched(FileReleased::class);
});

test('saved instance is valid', function () {
    $testing = ProductPropertyValue::factory()->createOne();

    expect($testing->isValid())->toBeTrue();
});

test('deleted is not valid', function () {
    $testing = ProductPropertyValue::factory()->createOne();
    $testing->forceDelete();

    expect($testing->isValid())->toBeFalse();
});

test('trashed is not valid', function () {
    $testing = ProductPropertyValue::factory()->createOne();
    $testing->delete();

    expect($testing->isValid())->toBeFalse();
});
