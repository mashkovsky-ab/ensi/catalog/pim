<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

/**
 * @method ProductImage createOne(array $fields = [])
 * @method ProductImage makeOne(array $fields = [])
 * @method ProductImage|ProductImage[]|Collection create(array $fields = [], $parent = null)
 */
class ProductImageFactory extends Factory
{
    protected $model = ProductImage::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        $fileId = $this->faker->unique()->numberBetween(100, 999);

        return [
            'file' => "path/to/image_{$fileId}.png",
            'product_id' => Product::factory(),
            'sort' => $this->faker->numberBetween(1, 500),
            'name' => $this->faker->optional->sentence(3),
        ];
    }

    public function owned(Product $product): self
    {
        return $this->state(function () use ($product) {
            return [
                'product_id' => $product->getKey(),
            ];
        });
    }

    public function external(): self
    {
        return $this->state(function () {
            return [
                'url' => $this->faker->url,
                'file' => null,
            ];
        });
    }

    public function published(): self
    {
        return $this->afterCreating(fn (ProductImage $value) => $value->publish());
    }
}
