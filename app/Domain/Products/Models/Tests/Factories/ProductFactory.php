<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductFlagValue;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

/**
 * @method Product createOne(array $fields = [])
 * @method Product makeOne(array $fields = [])
 * @method Product|Product[]|Collection create(array $fields = [], $parent = null)
 */
class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug,
            'description' => $this->faker->text,
            'type' => $this->faker->randomElement(ProductType::cases()),
            'status_id' => $this->faker->randomElement(ProductStatus::managedStatuses()),
            'allow_publish' => true,

            'external_id' => $this->faker->unique()->numerify('######'),
            'barcode' => $this->faker->ean13,
            'vendor_code' => $this->faker->numerify('###-###-###'),

            'weight' => $this->faker->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->randomFloat(3, 1, 100),
            'length' => $this->faker->randomFloat(2, 1, 1000),
            'height' => $this->faker->randomFloat(2, 1, 1000),
            'width' => $this->faker->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,

            'category_id' => Category::factory(),
            'brand_id' => Brand::factory(),
        ];
    }

    public function inCategory(Category $category): self
    {
        return $this->state(fn () => ['category_id' => $category->id]);
    }

    public function withFlags(ProductFlag ...$flags): self
    {
        return $this->afterCreating(function (Product $product) use ($flags) {
            $relation = $product->flags();

            foreach ($flags as $flag) {
                ProductFlagValue::create(['value' => $flag->value, 'product_id' => $product->id]);
            }
        });
    }

    public function deleted(): self
    {
        return $this->state(['status_id' => ProductStatus::DELETED]);
    }
}
