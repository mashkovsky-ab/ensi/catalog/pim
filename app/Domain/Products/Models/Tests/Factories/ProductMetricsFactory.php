<?php

namespace App\Domain\Products\Models\Tests\Factories;

use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductMetrics;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

/**
 * @method ProductMetrics createOne(array $fields = [])
 * @method ProductMetrics makeOne(array $fields = [])
 * @method ProductMetrics|ProductMetrics[]|Collection create(array $fields = [])
 */
class ProductMetricsFactory extends Factory
{
    protected $model = ProductMetrics::class;

    public function definition(): array
    {
        return array_merge(
            $this->generateStat(0),
            [
                'product_id' => Product::factory(),
                'category' => $this->faker->randomElement(MetricsCategory::cases()),
            ]
        );
    }

    public function errors(int $count): self
    {
        return $this->state(fn () => $this->generateStat($count));
    }

    public function forProduct(Product $product): self
    {
        return $this->state(['product_id' => $product->id]);
    }

    private function generateStat(int $errors): array
    {
        $total = $this->faker->numberBetween($errors + 2, $errors + 20);

        return [
            'total' => $total,
            'filled' => $this->faker->numberBetween(0, $total - $errors),
            'errors' => $errors,
        ];
    }
}
