<?php

namespace App\Domain\Products\Models;

use App\Domain\Classifiers\Enums\ProductFlag;
use App\Domain\Classifiers\Models\ProductFlagSettings;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Признак товара.
 *
 * @property int $product_id Идентификатор товара
 * @property ProductFlag $value Значение флага из перечисления ProductFlagsEnum
 *
 * @property-read ProductFlagSettings $flag
 *
 * @method static static create(array $fields)
 * @method static Builder|static whereProductId(int $value)
 */
class ProductFlagValue extends Model
{
    private const FILLABLE = ['value', 'product_id'];

    protected $table = 'product_flag_values';
    protected $fillable = self::FILLABLE;
    public $timestamps = false;

    protected $casts = [
        'product_id' => 'int',
        'value' => ProductFlag::class,
    ];

    public function flag(): BelongsTo
    {
        return $this->belongsTo(ProductFlagSettings::class, 'value');
    }
}
