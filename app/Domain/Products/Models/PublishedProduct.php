<?php

namespace App\Domain\Products\Models;

use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Enums\ProductType;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Опубликованный товар.
 *
 * @property bool $allow_publish Признак активности для витрины
 * @property string|null $external_id Идентификатор товара во внешней системе
 * @property int $category_id Идентификатор категории
 * @property ProductType $type Тип товара (весовой, штучный, ...) из перечисления ProductTypeEnum
 * @property ProductStatus $status_id Статус товара из перечисления ProductStatus
 * @property string|null $status_comment Комментарий к статусу
 * @property string|null $barcode Штрихкод (EAN)
 * @property string|null $vendor_code Артикул
 * @property string $name Название
 * @property string $code ЧПУ код
 * @property string|null $description Описание
 * @property float|null $weight Вес нетто (кг)
 * @property float|null $weight_gross Вес брутто (кг)
 * @property float|null $width Ширина (мм)
 * @property float|null $height Высота (мм)
 * @property float|null $length Длина (мм)
 * @property int|null $brand_id Идентификатор бренда
 * @property bool $is_adult Товар 18+
 * @property string|null $main_image URL основной картинки
 *
 * @property-read Collection|PublishedPropertyValue[] $properties Опубликованные значения свойств
 * @property-read Collection<PublishedImage> $images Опубликованные изображения
 */
class PublishedProduct extends Model
{
    use ExpandsProduct;

    // Идентификатор такой же как у товара.
    public $incrementing = false;

    protected $casts = [
        'weight' => 'float',
        'weight_gross' => 'float',
        'width' => 'float',
        'height' => 'float',
        'length' => 'float',
        'category_id' => 'int',
        'brand_id' => 'int',
        'status_id' => ProductStatus::class,
        'type' => ProductType::class,
        'is_adult' => 'bool',
        'allow_publish' => 'bool',
    ];

    public function properties(): HasMany
    {
        return $this->hasMany(PublishedPropertyValue::class, 'product_id');
    }

    public function images(): HasMany
    {
        return $this->hasMany(PublishedImage::class, 'product_id')
            ->orderBy('product_id')
            ->orderBy('sort');
    }
}
