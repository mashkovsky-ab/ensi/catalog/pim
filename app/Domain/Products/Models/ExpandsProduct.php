<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Models\Category;
use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Concerns\FiltersByTimestamps;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @mixin Model
 *
 * @property-read Category $category Категория
 * @property-read Brand|null $brand Бренд
 * @property-read ProductStatus $status Статус товара
 *
 * @method static Builder|self whereBrandId(int $brandId)
 */
trait ExpandsProduct
{
    use FiltersByTimestamps;

    public function status(): BelongsTo
    {
        return $this->belongsTo(ProductStatus::class, 'status_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
}
