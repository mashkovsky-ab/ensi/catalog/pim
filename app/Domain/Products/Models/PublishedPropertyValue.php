<?php

namespace App\Domain\Products\Models;

/**
 * Опубликованное значение атрибута товара.
 */
class PublishedPropertyValue extends BasePropertyValue
{
    protected $table = 'published_property_values';

    public function suspend(): void
    {
        if (!$this->trashed()) {
            $this->delete();
        }
    }

    public function resume(): void
    {
        if ($this->trashed()) {
            $this->restore();
        }
    }
}
