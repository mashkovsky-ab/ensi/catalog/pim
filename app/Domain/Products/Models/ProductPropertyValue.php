<?php

namespace App\Domain\Products\Models;

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Data\PropertyValueHolder;
use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Products\Models\Tests\Factories\ProductPropertyValueFactory;
use App\Domain\Support\Concerns\SupportsPublishing;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Webmozart\Assert\Assert;

/**
 * Класс-модель для сущности "Значение атрибута товара"
 *
 * @property-read Property $property Свойство
 * @property-read Product $product Товар
 * @property-read PropertyDirectoryValue|null $directoryValue Вариант значения свойства-справочника
 * @property-read PublishedPropertyValue|null $release Опубликованное значение
 */
class ProductPropertyValue extends BasePropertyValue implements PropertyValueHolder
{
    use SupportsPublishing;

    /**
     * @var array Заполняемые поля модели
     */
    const FILLABLE = [
        'type',
        'value',
        'name',
        'directory_value_id',
    ];

    protected $fillable = self::FILLABLE;

    protected $table = 'product_property_values';

    public static function make(Product|int $product, Property $property, mixed $value = null, ?string $name = null): self
    {
        $instance = new static();
        $instance->product_id = is_int($product) ? $product : $product->id;
        $instance->property_id = $property->id;

        if ($value instanceof PropertyDirectoryValue) {
            $instance->setDirectoryValue($value);
        } elseif ($value !== null) {
            $instance->setOwnValue(PropertyValue::make($property->type, $value), $name);
        }

        return $instance;
    }

    public function setValue(PropertyValue $value): static
    {
        Assert::false($value->isNull(), 'Требуется ненулевое значение');

        $this->type = $value->type();
        $this->value = (string)$value;

        return $this;
    }

    public function setDirectoryValue(PropertyDirectoryValue $reference): static
    {
        if ($reference->id === $this->directory_value_id) {
            return $this;
        }

        $this->setValue($reference->getValue());
        $this->name = $reference->name;
        $this->directory_value_id = $reference->id;

        return $this;
    }

    public function setOwnValue(PropertyValue $value, ?string $name = null): static
    {
        $this->setValue($value);
        $this->name = $name;
        $this->directory_value_id = null;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function isDirectory(): bool
    {
        return $this->directory_value_id !== null;
    }

    public function hash(): string
    {
        $value = $this->directory_value_id ?? $this->value;

        return "{$this->property_id}#{$this->type}#{$value}";
    }

    protected function newRelease(): Model
    {
        $release = new PublishedPropertyValue();

        $release->product_id = $this->product_id;
        $release->property_id = $this->property_id;

        return $release;
    }

    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function directoryValue(): BelongsTo
    {
        return $this->belongsTo(PropertyDirectoryValue::class, 'directory_value_id');
    }

    /** @noinspection PhpUndefinedMethodInspection */
    public function release(): BelongsTo
    {
        return $this->belongsTo(PublishedPropertyValue::class, 'id', 'id')
            ->withTrashed();
    }

    public static function factory(): ProductPropertyValueFactory
    {
        return ProductPropertyValueFactory::new();
    }
}
