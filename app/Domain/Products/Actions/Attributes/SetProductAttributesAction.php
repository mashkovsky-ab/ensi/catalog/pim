<?php

namespace App\Domain\Products\Actions\Attributes;

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\CategoryPropertyView;
use App\Domain\Categories\Models\PropertyDirectoryValue;
use App\Domain\Categories\Models\PropertyType;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductPropertyValue;
use App\Domain\Support\Concerns\InteractsWithModels;
use App\Domain\Support\Models\TempFile;
use App\Exceptions\IllegalOperationException;
use Generator;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class SetProductAttributesAction
{
    use InteractsWithModels;

    /**
     * @param Product $product
     * @param array $values Значения атрибутов. Каждое значение в виде массива с ключами property_id, value, ...
     * @param bool $replace Удалять значения не указанных в $values атрибутов
     * @return int Количество измененных значений
     */
    public function execute(Product $product, array $values, bool $replace): int
    {
        $changes = 0;
        $relation = $product->properties();

        $availableProperties = $product->availableProperties->keyBy('property_id');

        $existingValues = $this->loadExistingValues($relation, $availableProperties);
        $newValues = collect($values)->groupBy('property_id');

        foreach ($this->makeValues($newValues, $availableProperties) as $newValue) {
            $value = $this->prepareExistingValue($newValue, $existingValues) ?? $newValue;

            if ($value->exists && !$value->isDirty()) {
                continue;
            }

            $this->saveRelatedOrThrow($relation, $value);
            $changes++;
        }

        foreach ($existingValues as $value) {
            if ($value->trashed()) {
                continue;
            }

            if (!$replace && !$newValues->has($value->property_id)) {
                continue;
            }

            $this->deleteOrThrow($value);
            $changes++;
        }

        return $changes;
    }

    private function makeValues(Collection $source, Collection $availableProperties): Generator
    {
        foreach ($source as $propertyId => $values) {
            /** @var CategoryPropertyView $property */
            $property = $availableProperties->get($propertyId);

            if ($property === null) {
                continue;
            }

            throw_if(
                !$property->is_multiple && $values->count() > 1,
                IllegalOperationException::class,
                "Атрибут [{$property->id}] \"{$property->name}\" не поддерживает множественные значения"
            );

            foreach ($values as $fields) {
                yield $this->makeValue($property, $fields);
            }
        }
    }

    private function makeValue(CategoryPropertyView $property, array $fields): ProductPropertyValue
    {
        $value = new ProductPropertyValue();
        $value->property_id = $property->property_id;

        if ($property->has_directory) {
            throw_unless(
                array_key_exists('directory_value_id', $fields),
                IllegalOperationException::class,
                "Атрибут [{$property->id}] \"{$property->name}\" требует значение из справочника"
            );

            $value->setDirectoryValue(PropertyDirectoryValue::findOrFail($fields['directory_value_id']));

            return $value;
        }

        throw_if(
            array_key_exists('directory_value_id', $fields),
            IllegalOperationException::class,
            "Атрибут [{$property->id}] \"{$property->name}\" не является справочником"
        );

        if (array_key_exists('preload_file_id', $fields)) {
            throw_unless(
                $property->type->is(PropertyType::IMAGE()),
                IllegalOperationException::class,
                "Атрибут [{$property->id}] \"{$property->name}\" не поддерживает файл в качестве значения"
            );

            $fields['value'] = TempFile::grab($fields['preload_file_id']);
            unset($fields['preload_file_id']);
        }

        $value->setOwnValue(
            PropertyValue::make($property->type, $fields['value']),
            $fields['name'] ?? null
        );

        return $value;
    }

    /**
     * @param HasMany $relation
     * @param Collection|CategoryPropertyView[] $availableProperties
     * @return ProductPropertyValue[]|Collection
     */
    private function loadExistingValues(HasMany $relation, Collection $availableProperties): Collection
    {
        return $relation->withTrashed()
            ->get()
            ->filter(fn (ProductPropertyValue $value) => $availableProperties->has($value->property_id))
            ->keyBy(fn (ProductPropertyValue $value) => $value->hash());
    }

    private function prepareExistingValue(ProductPropertyValue $sample, Collection $existingValues): ?ProductPropertyValue
    {
        /** @var ProductPropertyValue|null $value */
        $value = $existingValues->pull($sample->hash());

        if ($value === null) {
            return null;
        }

        if ($value->trashed()) {
            $value->restore();
        }

        return $value->fill($sample->getAttributes());
    }
}
