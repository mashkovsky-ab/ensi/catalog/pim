<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Products\Actions\Attributes\SetProductAttributesAction;
use App\Domain\Products\Actions\Images\SetProductImagesAction;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Concerns\InteractsWithModels;

class SetProductDataAction
{
    use InteractsWithModels;

    public function __construct(
        private readonly SetProductImagesAction $imagesAction,
        private readonly SetProductAttributesAction $attributesAction
    ) {
    }

    public function execute(Product $product, array $fields, bool $replace): int
    {
        $changes = 0;
        $fillableFields = $replace
            ? $this->makeReplaceFields($product, $fields)
            : $fields;

        $product->fill($fillableFields);

        if ($product->isDirty()) {
            $this->saveOrThrow($product);
            $changes++;
        }

        if (isset($fields['images'])) {
            $changes += $this->imagesAction->execute($product, $fields['images'], $replace);
            $product->load('images');
        }

        if (isset($fields['attributes'])) {
            $changes += $this->attributesAction->execute($product, $fields['attributes'], $replace);
            $product->load('properties');
        }

        return $changes;
    }

    private function makeReplaceFields(Product $product, array $fields): array
    {
        $notNullFields = ['allow_publish', 'category_id', 'status_id'];

        $fillable = array_filter($product->getFillable(), function (string $name) use ($notNullFields, $fields) {
            return !in_array($name, $notNullFields) || array_key_exists($name, $fields);
        });

        return data_combine_assoc($fillable, $fields);
    }
}
