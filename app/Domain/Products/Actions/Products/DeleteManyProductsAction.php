<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Support\Concerns\HandlesMassOperation;

class DeleteManyProductsAction
{
    use HandlesMassOperation;

    public function __construct(private DeleteProductAction $deleteAction)
    {
    }

    public function execute(array $productIds): void
    {
        $this->each($productIds, function (int $productId) {
            $this->deleteAction->execute($productId);
        });
    }
}
