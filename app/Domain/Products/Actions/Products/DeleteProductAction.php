<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Classifiers\Enums\ProductStatus;
use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Concerns\AppliesToAggregate;

/**
 * @extends AppliesToAggregate<Product>
 */
class DeleteProductAction
{
    use AppliesToAggregate;

    public function execute(int $productId): void
    {
        $this->patch($productId, ['status_id' => ProductStatus::DELETED]);

        ProductInvalidated::dispatch($productId);
    }

    protected function createModel(): Product
    {
        return new Product();
    }
}
