<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Support\Actions\UploadFileAction;
use App\Domain\Support\Models\TempFile;
use Illuminate\Http\UploadedFile;

class PreloadImageAction
{
    public function __construct(private UploadFileAction $action)
    {
    }

    public function execute(UploadedFile $uploadedFile): TempFile
    {
        return $this->action->execute($uploadedFile, 'products');
    }
}
