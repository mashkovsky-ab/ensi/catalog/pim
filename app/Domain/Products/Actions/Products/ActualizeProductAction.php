<?php

namespace App\Domain\Products\Actions\Products;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Publication\Data\ProductAggregate;
use App\Domain\Products\Publication\ProductPublisherFactory;
use App\Domain\Support\Concerns\AppliesToAggregate;

/**
 * @extends AppliesToAggregate<Product>
 */
class ActualizeProductAction
{
    use AppliesToAggregate;

    public function __construct(private readonly ProductPublisherFactory $publisherFactory)
    {
    }

    public function execute(int $productId): void
    {
        $this->apply($productId, function (Product $product) {
            $aggregate = ProductAggregate::fromDraft($product);

            $this->publisherFactory->create()->publish($aggregate, true);
        });
    }

    protected function createModel(): Product
    {
        return new Product();
    }
}
