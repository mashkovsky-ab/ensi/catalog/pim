<?php

namespace App\Domain\Products\Actions\Images;

use App\Domain\Products\Events\ProductInvalidated;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Concerns\AppliesToAggregate;
use Illuminate\Support\Collection;

class PatchProductImagesAction
{
    use AppliesToAggregate;

    public function __construct(private readonly SetProductImagesAction $action)
    {
    }

    public function execute(int $productId, array $images): Collection
    {
        $product = $this->apply($productId, function (Product $product) use ($images) {
            $this->action->execute($product, $images, false);

            return $product;
        });

        ProductInvalidated::dispatch($product->id);

        return $product->images()->get();
    }

    protected function createModel(): Product
    {
        return new Product();
    }
}
