<?php

namespace App\Domain\Products\Actions\Variants;

use App\Domain\Categories\Models\Property;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\VariantGroup;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

abstract class BaseVariantGroupAction
{
    use AppliesToAggregate;

    private static $validationMessages = [
        'distinct' => 'Комбинации значений атрибутов товаров не уникальны',
    ];

    protected function createModel(): Model
    {
        return new VariantGroup();
    }

    protected function updateVariantGroup(VariantGroup $group, array $fields): void
    {
        $attributeIds = $fields['attribute_ids'] ?? $group->attribute_ids;
        $productIds = $fields['product_ids'] ?? $group->product_ids;

        $products = $this->validateProducts($attributeIds, $productIds);
        $group->fill($fields);

        $this->saveOrThrow($group);

        $this->syncProducts($group, $products);
        $this->syncAttributes($group);
    }

    protected function validateProducts(array $attribute_ids, array $product_ids): Collection
    {
        $attributes = Property::findOrFail($attribute_ids);
        $products = Product::query()->with(['properties'])->findOrFail($product_ids);

        $values = $products->map(fn (Product $product) => $product->variantValues($attributes))->toArray();

        Validator::make(
            ['product_ids' => $values],
            ['product_ids.*' => ['distinct']],
            self::$validationMessages
        )->validate();

        return $products;
    }

    protected function syncProducts(VariantGroup $group, Collection $products): void
    {
        $products->each(fn (Product $product) => $this->attachProduct($product, $group->id));

        $group->products()
            ->whereNotIn('id', $group->product_ids)
            ->each(fn (Product $product) => $this->attachProduct($product, null));
    }

    protected function syncAttributes(VariantGroup $group): void
    {
        $group->properties()->sync($group->attribute_ids);
    }

    protected function attachProduct(Product $product, ?int $groupId): void
    {
        $product->variant_group_id = $groupId;
        $this->saveOrThrow($product);
    }
}
