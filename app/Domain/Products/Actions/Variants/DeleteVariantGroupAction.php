<?php

namespace App\Domain\Products\Actions\Variants;

use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\VariantGroup;

class DeleteVariantGroupAction extends BaseVariantGroupAction
{
    public function execute(int $groupId): void
    {
        $this->delete($groupId, function (VariantGroup $group) {
            $group->properties()->sync([]);

            $group->products()->each(
                fn (Product $product) => $this->attachProduct($product, null)
            );
        });
    }
}
