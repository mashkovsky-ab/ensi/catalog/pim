<?php

namespace App\Domain\Products\Actions\Variants;

use App\Domain\Products\Models\VariantGroup;

class CreateVariantGroupAction extends BaseVariantGroupAction
{
    public function execute(array $fields): VariantGroup
    {
        return $this->updateOrCreate(null, function (VariantGroup $group) use ($fields) {
            $this->updateVariantGroup($group, $fields);
        });
    }
}
