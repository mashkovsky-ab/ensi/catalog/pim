<?php

namespace App\Domain\Products\Actions\Variants;

use App\Domain\Products\Models\VariantGroup;

class ReplaceVariantGroupAction extends BaseVariantGroupAction
{
    public function execute(int $groupId, array $fields): VariantGroup
    {
        return $this->updateOrCreate($groupId, function (VariantGroup $group) use ($fields) {
            $this->updateVariantGroup($group, data_combine_assoc($group->getFillable(), $fields));
        });
    }
}
