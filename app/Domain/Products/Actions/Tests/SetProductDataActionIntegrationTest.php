<?php

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyType;
use App\Domain\Products\Actions\Products\SetProductDataAction;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\ProductImage;
use App\Domain\Products\Models\ProductPropertyValue;
use Illuminate\Support\Arr;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\travel;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('products', 'integration');

test('replace fields', function () {
    $product = Product::factory()->createOne();
    $source = Product::factory()->inCategory($product->category)->makeOne();
    $fields = Arr::except($source->getAttributes(), ['weight', 'height']);

    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, true);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        $product->getTable(),
        ['id' => $product->id, 'name' => $fields['name'], 'weight' => null, 'height' => null]
    );
});

test('patch fields', function () {
    $product = Product::factory()->createOne();
    $fields = ['name' => 'foo'];

    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        $product->getTable(),
        ['id' => $product->id, 'name' => 'foo', 'weight' => $product->weight]
    );
});

test('skip not changed', function () {
    $product = Product::factory()->createOne();
    $fields = ['name' => $product->name, 'allow_publish' => $product->allow_publish];

    travel(1)->minutes();
    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, false);

    expect($changes)->toBe(0);
    assertDatabaseHas($product->getTable(), ['id' => $product->id, 'updated_at' => $product->updated_at]);
});

test('add image', function () {
    $product = Product::factory()->createOne();
    $fields = ['images' => [['url' => 'https://example.com?img=10']]];

    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        (new ProductImage())->getTable(),
        ['product_id' => $product->id, 'url' => 'https://example.com?img=10']
    );
});

test('add attribute value', function () {
    $product = Product::factory()->createOne();
    $property = Property::factory()
        ->withType(PropertyType::STRING())
        ->actual($product->category)
        ->createOne();

    $fields = ['attributes' => [
        ['property_id' => $property->id, 'value' => 'foo', 'name' => 'bar'],
    ]];

    $changes = resolve(SetProductDataAction::class)->execute($product, $fields, false);

    expect($changes)->toBe(1);
    assertDatabaseHas(
        (new ProductPropertyValue())->getTable(),
        ['product_id' => $product->id, 'property_id' => $property->id, 'value' => 'foo', 'name' => 'bar']
    );
});
