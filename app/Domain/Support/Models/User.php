<?php

namespace App\Domain\Support\Models;

use Ensi\LaravelAuditing\Contracts\Principal;

/**
 * @property int $id Идентификатор пользователя
 * @property string $name Наименование пользователя
 */
class User extends Model implements Principal
{
    const FILLABLE = ['id', 'name'];

    protected $fillable = self::FILLABLE;

    public $incrementing = false;

    /**
     * Возвращает наименование субъекта.
     *
     * @param string|null $value
     * @return string
     */
    protected function getNameAttribute(?string $value): string
    {
        return $value ?? 'Пользователь #'.$this->getKey();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAuthIdentifier(): int
    {
        return (int)$this->getKey();
    }

    public function getUserIdentifier(): ?int
    {
        return (int)$this->getKey();
    }
}
