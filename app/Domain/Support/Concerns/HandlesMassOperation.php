<?php

namespace App\Domain\Support\Concerns;

use App\Domain\Support\MassOperationResult;
use Closure;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

trait HandlesMassOperation
{
    public function each(array $ids, Closure $handler): MassOperationResult
    {
        $result = new MassOperationResult();

        foreach ($ids as $entityId) {
            try {
                $handler($entityId);
                $result->success($entityId);
            } catch (ModelNotFoundException) {
                // Игнорируем
            } catch (Exception $exception) {
                $result->error($entityId, $exception);
            }
        }

        return $result;
    }
}
