<?php

namespace App\Domain\Support\Concerns;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * @method static Builder|static whereCreatedAt(CarbonInterface $date)
 * @method static Builder|static whereUpdatedAt(CarbonInterface $date)
 * @method static Builder|static createdAtFrom(CarbonInterface $date)
 * @method static Builder|static updatedAtFrom(CarbonInterface $date)
 * @method static Builder|static createdAtTo(CarbonInterface $date)
 * @method static Builder|static updatedAtTo(CarbonInterface $date)
 */
trait FiltersByTimestamps
{
    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }

    public function scopeUpdatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('updated_at', '>=', $date);
    }

    public function scopeUpdatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('updated_at', '<=', $date);
    }
}
