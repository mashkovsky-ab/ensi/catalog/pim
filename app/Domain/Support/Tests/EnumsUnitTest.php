<?php

use App\Domain\Categories\Models\PropertyType;
use App\Http\ApiV1\OpenApiGenerated\Enums\PropertyTypeEnum;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('support', 'unit');

test('PropertyTypeEnum', function ($value) {
    expect(PropertyType::coerce($value))->not->toBeNull();
})->with(PropertyTypeEnum::cases());
