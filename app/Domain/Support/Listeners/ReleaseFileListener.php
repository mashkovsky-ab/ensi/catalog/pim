<?php

namespace App\Domain\Support\Listeners;

use App\Domain\Support\Events\FileReleased;
use App\Domain\Support\Models\TempFile;

class ReleaseFileListener
{
    public function handle(FileReleased $event): void
    {
        $existing = TempFile::wherePath($event->path)->first();
        if ($existing !== null) {
            return;
        }

        TempFile::create(['path' => $event->path]);
    }
}
