<?php

namespace App\Domain\Classifiers\Models;

use App\Domain\Support\Models\Model;

/**
 * @property string $name
 */
class ProductFlagSettings extends Model
{
    public const FILLABLE = ['name'];

    protected $table = 'product_flags';
    protected $fillable = self::FILLABLE;
    public $incrementing = false;
}
