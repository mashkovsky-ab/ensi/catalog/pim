<?php

namespace App\Domain\Classifiers\Enums;

enum ProductStatus : int
{
    case NEW = 1;
    case AGREED = 2;
    case REJECTED = 3;
    case LOCKED = 4;
    case CLOSED = 5;
    case DELETED = 6;

    public static function managedStatuses(): array
    {
        return [
            self::NEW,
            self::AGREED,
            self::REJECTED,
            self::LOCKED,
            self::CLOSED,
        ];
    }
}
