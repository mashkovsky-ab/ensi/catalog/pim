<?php

namespace App\Domain\Classifiers\Actions;

use App\Domain\Classifiers\Models\ProductStatusSettings;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class ReplaceProductStatusAction
{
    use AppliesToAggregate;

    public function execute(int $id, array $fields): ProductStatusSettings
    {
        return $this->replace($id, $fields);
    }

    protected function createModel(): Model
    {
        return new ProductStatusSettings();
    }
}
