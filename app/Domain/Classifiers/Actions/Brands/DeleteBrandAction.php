<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Products\Models\Product;
use App\Domain\Products\Models\PublishedProduct;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Exceptions\IllegalOperationException;

/**
 * @extends AppliesToAggregate<Brand>
 */
class DeleteBrandAction
{
    use AppliesToAggregate;

    public function execute(int $brandId): void
    {
        $this->delete($brandId, function (Brand $brand) {
            $hasProducts = Product::whereBrandId($brand->id)->exists()
                || PublishedProduct::whereBrandId($brand->id)->exists();

            throw_if(
                $hasProducts,
                IllegalOperationException::class,
                "Бренд [{$brand->id}] \"{$brand->name}\" используется в товарах. Удаление запрещено."
            );
        });
    }

    protected function createModel(): Model
    {
        return new Brand();
    }
}
