<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

/**
 * @extends AppliesToAggregate<Brand>
 */
class ReplaceBrandAction
{
    use AppliesToAggregate;

    public function execute(int $brandId, array $fields): Brand
    {
        return $this->replace($brandId, $fields);
    }

    protected function createModel(): Model
    {
        return new Brand();
    }
}
