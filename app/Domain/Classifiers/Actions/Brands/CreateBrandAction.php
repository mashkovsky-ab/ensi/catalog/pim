<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

/**
 * @extends AppliesToAggregate<Brand>
 */
class CreateBrandAction
{
    use AppliesToAggregate;

    public function execute(array $fields): Brand
    {
        return $this->create($fields);
    }

    protected function createModel(): Model
    {
        return new Brand();
    }
}
