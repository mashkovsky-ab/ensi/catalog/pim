<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

/**
 * @extends AppliesToAggregate<Brand>
 */
class PatchBrandAction
{
    use AppliesToAggregate;

    public function execute(int $brandId, array $fields): Brand
    {
        return $this->patch($brandId, $fields);
    }

    protected function createModel(): Model
    {
        return new Brand();
    }
}
