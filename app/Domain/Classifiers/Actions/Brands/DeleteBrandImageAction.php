<?php

namespace App\Domain\Classifiers\Actions\Brands;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

/**
 * @extends AppliesToAggregate<Brand>
 */
class DeleteBrandImageAction
{
    use AppliesToAggregate;

    public function execute(int $brandId): Brand
    {
        return $this->updateOrCreate($brandId, function (Brand $brand) {
            $brand->logo_file = null;
        });
    }

    protected function createModel(): Model
    {
        return new Brand();
    }
}
