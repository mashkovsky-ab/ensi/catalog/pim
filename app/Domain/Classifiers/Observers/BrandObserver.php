<?php

namespace App\Domain\Classifiers\Observers;

use App\Domain\Classifiers\Models\Brand;
use App\Domain\Support\Events\FileReleased;

class BrandObserver
{
    public function updated(Brand $model): void
    {
        if ($model->wasChanged('logo_file')) {
            $this->releaseFile($model->getOriginal('logo_file'));
        }
    }

    public function deleted(Brand $model): void
    {
        $this->releaseFile($model->logo_file);
    }

    private function releaseFile(?string $file): void
    {
        if (!blank($file)) {
            FileReleased::dispatch($file);
        }
    }
}
