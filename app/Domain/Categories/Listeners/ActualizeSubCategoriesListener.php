<?php

namespace App\Domain\Categories\Listeners;

use App\Domain\Categories\Actions\Categories\ActualizeCategoryAction;
use App\Domain\Categories\Events\CategoryActualized;
use App\Domain\Categories\Models\Category;
use App\Support\Log\CategoriesLoggerAwareInterface;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Psr\Log\LoggerAwareTrait;

class ActualizeSubCategoriesListener implements ShouldQueue, CategoriesLoggerAwareInterface
{
    use LoggerAwareTrait;

    public bool $afterCommit = true;

    public function __construct(private ActualizeCategoryAction $action)
    {
    }

    public function handle(CategoryActualized $event): void
    {
        $categoryIds = Category::forParent($event->categoryId)->pluck('id');

        foreach ($categoryIds as $categoryId) {
            try {
                $this->action->execute($categoryId);
            } catch (Exception $e) {
                $message = "Не удалось актуализировать категорию: \"{$e->getMessage()}\"";
                $this->logger?->error($message, ['id' => $categoryId]);
            }
        }
    }
}
