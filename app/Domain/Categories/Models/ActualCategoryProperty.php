<?php

namespace App\Domain\Categories\Models;

use App\Domain\Categories\Data\CategoryPropertySample;
use App\Domain\Categories\Models\Tests\Factories\ActualCategoryPropertyFactory;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Модель действующего для категории атрибута.
 *
 * @property int $category_id Идентификатор категории
 * @property int $property_id Идентификатор атрибута
 * @property bool $is_required Атрибут обязателен для заполнения
 * @property bool $is_inherited Атрибут наследуется от родительской категории
 * @property bool $is_common Атрибут является общим для всех категорий
 *
 * @property-read Category $category
 */
class ActualCategoryProperty extends Model
{
    const TABLE_NAME = 'actual_category_properties';

    /**
     * @var array Заполняемые поля модели
     */
    const FILLABLE = ['property_id', 'category_id', 'is_required', 'is_inherited', 'is_common'];

    protected $fillable = self::FILLABLE;
    protected $table = self::TABLE_NAME;

    protected $casts = [
        'property_id' => 'int',
        'category_id' => 'int',
        'is_required' => 'bool',
        'is_inherited' => 'bool',
        'is_common' => 'bool',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function toSample(?bool $isInherited = null): CategoryPropertySample
    {
        return new CategoryPropertySample(
            $this->property_id,
            $this->is_required,
            $isInherited ?? $this->is_inherited
        );
    }

    public function fillFromSample(CategoryPropertySample $source): static
    {
        $this->fill([
            'property_id' => $source->propertyId,
            'is_required' => $source->isRequired,
            'is_inherited' => $source->isInherited,
            'is_common' => $source->isCommon,
        ]);

        return $this;
    }

    public static function fromSample(CategoryPropertySample $source): static
    {
        return (new static())->fillFromSample($source);
    }

    public static function factory(): ActualCategoryPropertyFactory
    {
        return ActualCategoryPropertyFactory::new();
    }
}
