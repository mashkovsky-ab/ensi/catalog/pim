<?php

namespace App\Domain\Categories\Models;

use App\Support\Enum;

/**
 * Поддерживаемые типы данных свойств.
 *
 * @method static static STRING()
 * @method static static INTEGER()
 * @method static static DOUBLE()
 * @method static static BOOLEAN()
 * @method static static DATETIME()
 * @method static static IMAGE()
 * @method static static COLOR()
 */
class PropertyType extends Enum
{
    const STRING = 'string';
    const INTEGER = 'integer';
    const DOUBLE = 'double';
    const BOOLEAN = 'boolean';
    const DATETIME = 'datetime';
    const IMAGE = 'image';
    const COLOR = 'color';

    public static function cases(): array
    {
        return [
            self::STRING,
            self::INTEGER,
            self::DOUBLE,
            self::BOOLEAN,
            self::DATETIME,
            self::IMAGE,
            self::COLOR,
        ];
    }
}
