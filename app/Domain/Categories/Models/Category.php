<?php

namespace App\Domain\Categories\Models;

use App\Domain\Categories\Models\Tests\Factories\CategoriesFactory;
use App\Domain\Products\Models\Product;
use App\Domain\Support\Models\Model;
use App\Exceptions\IllegalOperationException;
use Carbon\CarbonInterface;
use Cviebrock\EloquentSluggable\Sluggable;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Kalnoy\Nestedset\NodeTrait;

/**
 * Класс-модель для сущности "Категории товара".
 *
 * @property string $name Название
 * @property int $parent_id Идентификатор родительской категории
 * @property string $code ЧПУ код категории
 * @property bool $is_active Признак активности, устанавливаемый пользователями
 * @property bool $is_inherits_properties Признак наследования атрибутов родительской категории
 * @property bool $is_real_active Признак активности с учетом иерархии
 * @property array|null $full_code Массив кодов родительских и данной категории
 * @property CarbonInterface|null $actualized_at Метка времени последней актуализации категории и ее свойств
 *
 * @property-read Category|null $parent Родительская категория
 * @property-read Collection|Category[] $ancestors Родительские категории
 * @property-read Collection|Category[] $children Дочерние категории
 * @property-read Collection|Category[] $descendants Дочерние категории всех уровней
 *
 * @property-read Collection|CategoryPropertyView[] $properties Атрибуты товаров доступные в категории
 * @property-read Collection|BoundPropertyView[] $boundProperties Атрибуты, привязанные к данной категории
 * @property-read Collection|Product[] $products Принадлежащие категории товары
 * @property-read Collection|CategoryPropertyLink[] $propertyLinks Привязка атрибутов к категории
 * @property-read Collection|ActualCategoryProperty[] $actualProperties Привязка атрибутов к категории с учетом наследования
 *
 * @method static Collection|Category[] ancestorsAndSelf(int $id)
 * @method static Builder|self whereAncestorOf(int $categoryId, bool $includeSelf)
 * @method static Builder|self whereIsRoot()
 * @method static Builder|self forParent(int $parentId)
 */
class Category extends Model implements Auditable
{
    use NodeTrait;
    use Sluggable;
    use SupportsAudit;

    public const FILLABLE = ['name', 'code', 'parent_id', 'is_active', 'is_inherits_properties'];
    public const DEFAULT_CODE = 'default';

    protected $table = 'categories';
    protected $fillable = self::FILLABLE;

    protected $attributes = [
        'is_real_active' => false,
        'is_active' => false,
        'is_inherits_properties' => true,
    ];

    protected $casts = [
        'parent_id' => 'int',
        'is_active' => 'bool',
        'is_real_active' => 'bool',
        'is_inherits_properties' => 'bool',
        'full_code' => 'array',
    ];

    protected $dates = ['actualized_at'];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function properties(): HasMany
    {
        return $this->hasMany(CategoryPropertyView::class, 'category_id');
    }

    public function boundProperties(): HasMany
    {
        return $this->hasMany(BoundPropertyView::class, 'category_id');
    }

    public function propertyLinks(): HasMany
    {
        return $this->hasMany(CategoryPropertyLink::class, 'category_id');
    }

    public function actualProperties(): HasMany
    {
        return $this->hasMany(ActualCategoryProperty::class, 'category_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'code' => [
                'source' => 'name',
                'separator' => '_',
                'unique' => true,
                'maxLength' => 30,
            ],
        ];
    }

    public function replicate(array $except = null): self
    {
        $defaults = [
            $this->getParentIdName(),
            $this->getLftName(),
            $this->getRgtName(),
            'code',
        ];

        $except = empty($except) ? $defaults : array_unique(array_merge($except, $defaults));

        return parent::replicate($except);
    }

    public function getAuditExclude(): array
    {
        return [
            $this->getLftName(),
            $this->getRgtName(),
            'is_real_active',
            'full_code',
            'actualized_at',
        ];
    }

    public function scopeForParent(Builder $query, int $parentId): Builder
    {
        return $query->where($this->getParentIdName(), $parentId);
    }

    public function state(): CategoryState
    {
        if (!$this->exists) {
            throw new IllegalOperationException('Категория не сохранена в базе данных');
        }

        $state = new CategoryState($this->getOriginal());
        $state->id = $this->id;
        $state->exists = true;
        $state->syncOriginal();

        $attributes = $this->only($state->getFillable());
        $state->fill($attributes);

        return $state;
    }

    public static function getOrCreateDefault(): self
    {
        return self::firstOrCreate(['code' => self::DEFAULT_CODE], [
            'name' => 'Все товары',
            'is_active' => true,
        ]);
    }

    protected static function boot()
    {
        parent::boot();

//        self::deleted(function (self $category) {
//            Category::query()->fixTree();
//        });
    }

    public static function factory(): CategoriesFactory
    {
        return CategoriesFactory::new();
    }
}
