<?php

namespace App\Domain\Categories\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id Идентификатор категории
 * @property bool $is_real_active Признак активности с учетом иерархии
 * @property array|null $full_code Массив кодов родительских и данной категории
 * @property CarbonInterface|null $actualized_at Метка времени последней актуализации категории и ее свойств
 */
class CategoryState extends Model
{
    const CREATED_AT = null;

    const UPDATED_AT = 'actualized_at';

    protected $table = 'categories';

    protected $fillable = ['is_real_active', 'full_code', 'actualized_at'];

    protected $casts = [
        'is_real_active' => 'bool',
        'full_code' => 'array',
        'actualized_at' => 'timestamp',
    ];
}
