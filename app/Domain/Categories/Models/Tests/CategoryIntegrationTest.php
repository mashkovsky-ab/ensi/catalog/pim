<?php

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\travel;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class)->group('categories', 'integration');

test('creates audit records', function () {
    $category = Category::factory()->createOne();

    $category->is_active = !$category->is_active;
    $category->save();

    expect($category->audits()->count())->toBe(2);
});

test('bind property', function () {
    $category = Category::factory()->createOne();
    $link = ActualCategoryProperty::factory()
        ->forCategory($category)
        ->createOne();

    expect($category->properties->pluck('id'))
        ->toHaveCount(1)
        ->toContain($link->id);
});

test('make state', function () {
    $category = Category::factory()->actualized()->createOne();

    expect($category->state()->isDirty())->toBeFalse();
});

test('change state', function () {
    $category = Category::factory()->actualized()->createOne();
    $category->is_real_active = !$category->is_real_active;

    expect($category->state()->isDirty())->toBeTrue();
});

test('save state', function () {
    $category = Category::factory()->actualized()->createOne();
    $category->full_code = ['foo', 'bar'];

    travel(1)->minutes();
    $state = $category->state();
    $state->save();

    assertDatabaseHas($category->getTable(), [
        'id' => $category->id,
        'full_code' => json_encode($category->full_code),
        'is_real_active' => $category->is_real_active,
        'updated_at' => $category->updated_at,
        'actualized_at' => now(),
    ]);
});

test('touch state', function () {
    $category = Category::factory()->actualized()->createOne();

    travel(1)->minutes();
    $category->state()->touch();

    assertDatabaseHas($category->getTable(), [
        'id' => $category->id,
        'updated_at' => $category->updated_at,
        'actualized_at' => now(),
    ]);
});

test('saving state not add audit', function () {
    $category = Category::factory()->actualized()->createOne();
    $category->is_real_active = !$category->is_real_active;
    $expectedAuditCount = $category->audits()->count();

    $category->state()->save();

    expect($category->audits()->count())->toBe($expectedAuditCount);
});
