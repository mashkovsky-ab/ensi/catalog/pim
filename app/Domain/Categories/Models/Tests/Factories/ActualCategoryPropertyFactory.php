<?php

namespace App\Domain\Categories\Models\Tests\Factories;

use App\Domain\Categories\Models\ActualCategoryProperty;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

/**
 * @method ActualCategoryProperty createOne(array $fields = [])
 * @method ActualCategoryProperty makeOne(array $fields = [])
 * @method ActualCategoryProperty|ActualCategoryProperty[]|Collection create(array $fields = [], $parent = null)
 */
class ActualCategoryPropertyFactory extends Factory
{
    protected $model = ActualCategoryProperty::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return [
            'property_id' => Property::factory(),
            'category_id' => Category::factory(),
            'is_required' => $this->faker->boolean,
            'is_inherited' => false,
            'is_common' => false,
        ];
    }

    public function inherited(): self
    {
        return $this->state(['is_inherited' => true, 'is_common' => false]);
    }

    public function required(bool $value = true): self
    {
        return $this->state(['is_required' => $value]);
    }

    public function common(): self
    {
        return $this->state(['is_common' => true, 'is_inherited' => false]);
    }

    public function forCategory(Category $category): self
    {
        return $this->state(['category_id' => $category->id]);
    }

    public function forProperty(Property $property): self
    {
        return $this->state(['property_id' => $property->id]);
    }

    public function createFast(Category $category, Property $property, ?bool $isRequired = null): ActualCategoryProperty
    {
        $fields = [
            'category_id' => $category->id,
            'property_id' => $property->id,
        ];

        if ($isRequired !== null) {
            $fields['is_required'] = $isRequired;
        }

        return $this->createOne($fields);
    }
}
