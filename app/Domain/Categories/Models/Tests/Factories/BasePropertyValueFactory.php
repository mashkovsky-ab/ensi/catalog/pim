<?php

namespace App\Domain\Categories\Models\Tests\Factories;

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Date;

class BasePropertyValueFactory extends Factory
{
    public function definition(): array
    {
        $type = PropertyType::getRandomInstance();

        return [
            'property_id' => Property::factory()->withType($type),
            'value' => $this->generateValue($type),
            'name' => $this->faker->sentence(3),
            'type' => $type,
        ];
    }

    public function withType(PropertyType $type): static
    {
        return $this->state(function (array $fields) use ($type) {
            $result = [
                'type' => $type->value,
                'value' => $this->generateValue($type),
            ];

            $property = $fields['property_id'] ?? null;
            if ($property instanceof PropertyFactory) {
                $result['property_id'] = $property->withType($type);
            }

            return $result;
        });
    }

    public function forProperty(Property $property): static
    {
        return $this->state(function () use ($property) {
            return [
                'property_id' => $property->id,
                'type' => $property->type->value,
                'value' => $this->generateValue($property->type),
            ];
        });
    }

    protected function generateValue(PropertyType $type): mixed
    {
        return match ($type->value) {
            PropertyType::STRING => $this->faker->word,
            PropertyType::BOOLEAN => $this->faker->boolean === true ? 'true' : 'false',
            PropertyType::COLOR => '#' . dechex($this->faker->numberBetween(0x100000, 0xFFFFFF)),
            PropertyType::DATETIME => Date::make($this->faker->dateTime)->toJSON(),
            PropertyType::DOUBLE => $this->faker->randomFloat(4),
            PropertyType::INTEGER => $this->faker->randomNumber(),
            PropertyType::IMAGE => '/var/www/public/image_' . $this->faker->numberBetween(1, 1000) . '.jpg',
        };
    }
}
