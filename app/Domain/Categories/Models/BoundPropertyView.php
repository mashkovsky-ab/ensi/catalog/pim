<?php

namespace App\Domain\Categories\Models;

use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $category_id Идентификатор категории
 * @property int $property_id Идентификатор свойства
 * @property bool $is_inherited Свойство наследуется от родительской категории
 * @property bool $is_required Свойство обязательно для заполнения
 * @property bool $is_common Общее для всех категорий свойство
 * @property bool $is_system Свойство является системным
 *
 * @property string $name Название в системе
 * @property string $display_name Название на витрине
 * @property string $code Код атрибута
 * @property PropertyType $type Тип (string, integer, double, datetime, color)
 * @property string $hint_value Подсказка при заполнении для значения
 * @property string $hint_value_name Подсказка при заполнении для названия значения
 *
 * @property bool $is_active Активность свойства
 * @property bool $is_multiple Поддерживает множественные значения
 * @property bool $is_filterable Показывать в фильтре на витрине
 * @property bool $is_public Отображать на витрине
 * @property bool $has_directory Признак наличия справочника значений
 * @property bool $is_moderated Признак модерируемого атрибута
 *
 * @method static Builder|self whereCategoryId(int $categoryId)
 */
class BoundPropertyView extends Model
{
    protected $table = 'v_bound_category_properties';

    protected $fillable = [];

    protected $casts = [
        'is_inherited' => 'bool',
        'is_required' => 'bool',
        'is_common' => 'bool',
        'is_system' => 'bool',
        'category_id' => 'int',
        'property_id' => 'int',
        'type' => PropertyType::class,
        'is_multiple' => 'bool',
        'is_filterable' => 'bool',
        'is_active' => 'bool',
        'is_public' => 'bool',
        'has_directory' => 'bool',
        'is_moderated' => 'bool',
    ];
}
