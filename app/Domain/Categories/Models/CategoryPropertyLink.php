<?php

namespace App\Domain\Categories\Models;

use App\Domain\Categories\Data\CategoryPropertySample;
use App\Domain\Categories\Models\Tests\Factories\CategoryPropertyLinkFactory;
use App\Domain\Support\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

/**
 * Класс-модель для сущности "Привязки атрибутов товаров к категориям"
 *
 * @property int $property_id Идентификатор свойства
 * @property int $category_id Идентификатор категории
 * @property bool $is_required Свойство обязательно для заполнения
 *
 * @property-read Property $property - свойство
 * @property-read Category $category - категория
 *
 * @method static Builder|self forCategories(array $categoryIds)
 */
class CategoryPropertyLink extends Model
{
    const TABLE_NAME = 'category_property_links';

    /**
     * @var array Заполняемые поля модели
     */
    const FILLABLE = ['property_id', 'category_id', 'is_required'];

    protected $fillable = self::FILLABLE;
    protected $table = self::TABLE_NAME;

    protected $casts = [
        'property_id' => 'int',
        'category_id' => 'int',
        'is_required' => 'bool',
    ];

    /**
     * @return BelongsTo
     */
    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeForCategories(Builder $query, array $categoryIds): Builder
    {
        return $query->whereIn('category_id', $categoryIds);
    }

    public function toSample(): CategoryPropertySample
    {
        return new CategoryPropertySample($this->property_id, $this->is_required);
    }

    /**
     * Возвращает массив идентификаторов привязок действующих для категории свойств.
     *
     * @param int $categoryId
     * @return array|int[]
     * @deprecated
     */
    public static function selectEffectiveIds(int $categoryId): array
    {
        return Category::whereAncestorOf($categoryId, true)
            ->join(self::TABLE_NAME.' as link', 'categories.id', '=', 'link.category_id')
            ->select([
                'link.id as link_id',
                'link.property_id as property_id',
                DB::raw('CASE WHEN link.is_required THEN 0 else categories._rgt END as priority'),
            ])
            ->orderBy('priority')
            ->get()
            ->unique('property_id')
            ->pluck('link_id')
            ->toArray();
    }

    /**
     * @deprecated
     */
    public static function make(Category|int $category, Property|int $property, bool $isRequired = false): static
    {
        $instance = new static();
        $instance->category_id = is_int($category) ? $category : $category->id;
        $instance->property_id = is_int($property) ? $property : $property->id;
        $instance->is_required = $isRequired;

        return $instance;
    }

    public static function new(?int $propertyId = null): static
    {
        $instance = new static();

        if ($propertyId !== null) {
            $instance->property_id = $propertyId;
        }

        return $instance;
    }

    public static function factory(): CategoryPropertyLinkFactory
    {
        return CategoryPropertyLinkFactory::new();
    }
}
