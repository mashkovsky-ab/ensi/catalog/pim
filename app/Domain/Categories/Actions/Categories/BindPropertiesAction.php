<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Categories\Events\CategoryInvalidated;
use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\CategoryPropertyLink;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;
use App\Support\Actions\ActionDecorator;
use App\Support\Actions\DecoratesAction;
use Illuminate\Database\Eloquent\Relations\HasOneOrMany;
use Illuminate\Support\Collection;

class BindPropertiesAction
{
    use AppliesToAggregate;
    use DecoratesAction;

    public function execute(int $categoryId, bool $replace, array $properties): Category
    {
        [$category, $hasChanges] = $this->apply($categoryId, function (Category $category) use ($replace, $properties) {
            $hasChanges = $this->bind($category, $properties, $replace) > 0;

            return [$category, $hasChanges];
        });

        if ($hasChanges) {
            CategoryInvalidated::dispatch($category->id);
        }

        return $category;
    }

    public function enrichResult(): static|ActionDecorator
    {
        return $this->decorateResult(fn (Category $category) => $category->load('properties'));
    }

    protected function createModel(): Model
    {
        return new Category();
    }

    private function bind(Category $category, array $properties, bool $replace): int
    {
        $relation = $category->propertyLinks();
        $existing = $relation->get()->keyBy('property_id');

        return $this->updateLinks($existing, $properties, $relation)
            + $this->processUnchangedLinks($existing, $replace);
    }

    private function updateLinks(Collection $existing, array $properties, HasOneOrMany $relation): int
    {
        return collect($properties)
            ->reduce(function (int $carry, bool $isRequired, int $propertyId) use ($relation, $existing) {
                $link = $existing->pull($propertyId) ?? CategoryPropertyLink::new($propertyId);

                if ($link->exists && $link->is_required === $isRequired) {
                    return $carry;
                }

                $link->is_required = $isRequired;
                $this->saveRelatedOrThrow($relation, $link);

                return $carry + 1;
            }, 0);
    }

    private function processUnchangedLinks(Collection $links, bool $replace): int
    {
        return $replace
            ? $links->each(fn (CategoryPropertyLink $link) => $this->deleteOrThrow($link))->count()
            : 0;
    }
}
