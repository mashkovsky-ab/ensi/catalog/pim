<?php

namespace App\Domain\Categories\Actions\Categories;

use App\Domain\Categories\Models\Category;
use App\Domain\Categories\Models\Property;
use App\Support\Lazy;
use Illuminate\Support\Collection;

class ResolvePropertiesAction
{
    private Lazy|Collection $commonProperties;

    public function __construct()
    {
        $this->commonProperties = new Lazy(fn () => Property::common()->get());
    }

    public function execute(Category $category, ?Category $parent = null): Collection
    {
        $parent = $this->resolveParent($category, $parent);

        return $this->resolveExpectedProperties($category, $parent?->properties ?? collect());
    }

    private function resolveParent(Category $category, ?Category $parent): ?Category
    {
        if ($category->isRoot()) {
            return null;
        }

        return $parent?->id !== $category->parent_id
            ? $category->parent
            : $parent;
    }

    private function resolveExpectedProperties(Category $category, Collection $inheritedProperties): Collection
    {
        return new Collection();
    }
}
