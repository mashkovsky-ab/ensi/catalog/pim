<?php

namespace App\Domain\Categories\Actions\Properties;

use App\Domain\Categories\Models\Property;
use App\Domain\Support\Concerns\AppliesToAggregate;
use App\Domain\Support\Models\Model;

class CreatePropertyAction
{
    use AppliesToAggregate;

    public function execute(array $fields): Property
    {
        return $this->updateOrCreate(null, function (Property $property) use ($fields) {
            $property->fill($fields);
        });
    }

    protected function createModel(): Model
    {
        return new Property();
    }
}
