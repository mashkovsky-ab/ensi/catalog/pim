<?php

namespace App\Domain\Categories\Actions\DirectoryValues;

use App\Domain\Categories\Models\Property;
use App\Domain\Categories\Models\PropertyDirectoryValue;

class DeleteDirectoryValueAction
{
    use AppliesToDirectory;

    public function execute(int $valueId): void
    {
        $this->delete($valueId, fn (PropertyDirectoryValue $value, Property $property) => $this->checkProperty($property));
    }
}
