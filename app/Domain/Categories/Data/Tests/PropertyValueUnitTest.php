<?php

use App\Domain\Categories\Data\PropertyValue;
use App\Domain\Categories\Models\PropertyType;
use Carbon\CarbonInterface;
use Illuminate\Support\Facades\Date;
use Illuminate\Validation\ValidationException;
use Tests\UnitTestCase;

uses(UnitTestCase::class)->group('category', 'unit');

test('create primitive type', function (PropertyType $type, mixed $value, mixed $expected) {
    expect(PropertyValue::make($type, $value)->native())->toBe($expected);
})->with([
    'integer' => [PropertyType::INTEGER(), '1540', 1540],
    'double' => [PropertyType::DOUBLE(), '254.45', 254.45],
    'string' => [PropertyType::STRING(), 'foo', 'foo'],
    'true' => [PropertyType::BOOLEAN(), 'true', true],
    'false' => [PropertyType::BOOLEAN(), 'false', false],
    'image file' => [PropertyType::IMAGE(), '/var/www/public/main.jpg', '/var/www/public/main.jpg'],
]);

test('create datetime type', function (mixed $value, ?CarbonInterface $expected) {
    expect(PropertyValue::make(PropertyType::DATETIME(), $value)->native())->toEqual($expected);
})->with([
    'valid date' => ['2021-05-09', Date::make('2021-05-09')],
    'valid formatted date' => ['9.8.2021', Date::make('2021-08-09')],
    'valid datetime' => ['2021-05-09T15:47:11.000Z', Date::make('2021-05-09T15:47:11.000Z')],
    'valid instance' => [Date::make('2021-09-01T20:00:08'), Date::make('2021-09-01T20:00:08')],
    'invalid integer' => [19_000_524, null],
    'invalid string' => ['foo', null],
]);

test('create color type', function (mixed $value, ?string $expected) {
    expect(PropertyValue::make(PropertyType::COLOR(), $value)->native())->toEqual($expected);
})->with([
    'valid color' => ['#45454A', '#45454A'],
    'valid short' => ['#fff', '#fff'],
    'invalid value' => ['foo', null],
    'invalid character' => ['#0000S8', null],
]);

test('equal', function (PropertyValue $first, PropertyValue $second) {
    expect($first->equalTo($second))->toBeTrue();
})->with([
    'same type both nulls' => [PropertyValue::INTEGER(), PropertyValue::INTEGER()],
    'same type same value' => [PropertyValue::DOUBLE(50), PropertyValue::DOUBLE(50)],
    'datetime' => [PropertyValue::DATETIME('2021-09-01'), PropertyValue::DATETIME('2021-09-01')],
]);

test('not equal', function (PropertyValue $first, ?PropertyValue $second) {
    expect($first->equalTo($second))->toBeFalse();
})->with([
    'null value' => [PropertyValue::STRING('foo'), PropertyValue::STRING()],
    'null instance' => [PropertyValue::STRING('foo'), null],
    'different types' => [PropertyValue::COLOR('#aaa'), PropertyValue::STRING('#aaa')],
    'different values' => [PropertyValue::DATETIME('2021-09-01'), PropertyValue::DATETIME('2021-09-02')],
]);

test('validate success', function (PropertyType $type, mixed $value) {
    expect(PropertyValue::validate($type, $value)->equalTo(PropertyValue::make($type, $value)))->toBeTrue();
})->with([
    'integer' => [PropertyType::INTEGER(), 597],
    'double' => [PropertyType::DOUBLE(), 1024.14],
    'string' => [PropertyType::STRING(), 'foo'],
    'boolean' => [PropertyType::BOOLEAN(), false],
    'datetime' => [PropertyType::DATETIME(), '2021-09-24T19:34:11.000Z'],
    'image' => [PropertyType::IMAGE(), '/var/www/public/image1.gif'],
    'color' => [PropertyType::COLOR(), '#888888'],
]);

test('validate failed', function (PropertyType $type, mixed $value) {
    expect(fn () => PropertyValue::validate($type, $value))
        ->toThrow(ValidationException::class);
})->with([
    'integer' => [PropertyType::INTEGER(), 'foo'],
    'double' => [PropertyType::DOUBLE(), 'foo'],
    'boolean' => [PropertyType::BOOLEAN(), 15],
    'datetime' => [PropertyType::DATETIME(), 25],
    'color' => [PropertyType::COLOR(), '#19R'],
]);
