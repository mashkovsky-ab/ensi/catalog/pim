<?php

namespace App\Domain\Categories\Data;

interface PropertyValueHolder
{
    public function getValue(): PropertyValue;

    public function getName(): ?string;
}
