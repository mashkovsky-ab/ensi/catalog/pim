<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeVariantGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('variant_groups', function (Blueprint $table) {
            $table->dropColumn('property_id');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->bigInteger('variant_group_id')->unsigned()->nullable();

            $table->foreign('variant_group_id')->references('id')->on('variant_groups');
        });

        Schema::create('property_variant_group', function (Blueprint $table) {
            $table->bigInteger('variant_group_id')->unsigned();
            $table->bigInteger('property_id')->unsigned();

            $table->foreign('variant_group_id')->references('id')->on('variant_groups');
            $table->foreign('property_id')->references('id')->on('properties');
        });

        Schema::dropIfExists('product_variant_group');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variant_groups', function (Blueprint $table) {
            $table->bigInteger('property_id')->unsigned();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['variant_group_id']);
            $table->dropColumn('variant_group_id');
        });

        Schema::create('product_variant_group', function (Blueprint $table) {
            $table->bigInteger('variant_group_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();

            $table->foreign('variant_group_id')->references('id')->on('variant_groups');
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::dropIfExists('property_variant_group');
    }
}
