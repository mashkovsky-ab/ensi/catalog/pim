<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_property_links', function (Blueprint $table) {
            $table->index(['property_id']);
            $table->index(['category_id']);
        });

        Schema::table('product_property_values', function (Blueprint $table) {
            $table->string('type', 20);
            $table->index(['property_id']);
            $table->index(['product_id']);
        });

        Schema::table('variant_groups', function (Blueprint $table) {
            $table->json('attribute_ids');
            $table->json('product_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variant_groups', function (Blueprint $table) {
            $table->dropColumn('attribute_ids');
            $table->dropColumn('product_ids');
        });

        Schema::table('product_property_values', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropIndex(['property_id']);
            $table->dropIndex(['product_id']);
        });

        Schema::table('category_property_links', function (Blueprint $table) {
            $table->dropIndex(['property_id']);
            $table->dropIndex(['category_id']);
        });
    }
}
