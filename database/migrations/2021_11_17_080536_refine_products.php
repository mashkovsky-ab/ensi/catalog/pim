<?php

use App\Domain\Classifiers\Enums\ProductType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefineProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['manufacturer_id']);
            $table->dropForeign(['country_id']);
            $table->dropForeign(['product_type_id']);

            $table->dropColumn([
                'archive',
                'archive_comment',
                'archive_date',
                'is_new',
                'ingredients',
                'calories',
                'proteins',
                'fats',
                'carbohydrates',
                'storage_area',
                'storage_address',
                'shelf_life',
                'additional_info',
                'vat_rate',
                'manufacturer_id',
                'country_id',
                'product_type_id',
                'variant_group_id',
            ]);

            $table->timestamp('created_at', 6)->change();
            $table->timestamp('updated_at', 6)->change();

            $table->string('external_id')->nullable()->change();
            $table->dropUnique(['external_id']);

            $table->boolean('allow_publish')->default(false);
            $table->string('vendor_code')->nullable();
            $table->boolean('is_adult')->nullable();

            $table->foreignId('status_id')->default(1)->constrained('product_statuses');
            $table->string('status_comment')->nullable();
            $table->integer('type')->default(ProductType::PIECE->value);

            $table->decimal('weight', 18, 4)->nullable()->change();
            $table->decimal('weight_gross', 18, 4)->nullable()->change();
            $table->decimal('width', 18, 4)->nullable()->change();
            $table->decimal('height', 18, 4)->nullable()->change();
            $table->decimal('length', 18, 4)->nullable()->change();

            $table->unique(['code']);

            $table->index(['barcode']);
            $table->index(['vendor_code']);
            $table->index(['category_id']);
            $table->index(['external_id']);
            $table->index(['status_id']);
        });

        Schema::dropIfExists('manufacturers');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('product_types');

        Schema::table('product_images', function (Blueprint $table) {
            $table->dropColumn(['code']);

            $table->timestamp('created_at', 6)->change();
            $table->timestamp('updated_at', 6)->change();

            $table->integer('sort')->default(100);
            $table->string('url')->nullable();
            $table->string('name')->nullable();
            $table->softDeletes('deleted_at', 6);

            $table->index(['product_id', 'sort']);
            $table->index(['deleted_at', 'product_id']);
        });

        Schema::create('product_flag_values', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained('products');
            $table->foreignId('value')->constrained('product_flags');

            $table->index(['product_id', 'value']);
            $table->index(['value', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('product_flag_values');

        $this->restoreClassifiers();

        Schema::table('product_images', function (Blueprint $table) {
            $table->dropIndex(['product_id', 'sort']);
            $table->dropIndex(['deleted_at', 'product_id']);

            $table->dropSoftDeletes();
            $table->dropColumn([
                'sort',
                'name',
                'url',
            ]);

            $table->unsignedTinyInteger('code')->default(1);
            $table->timestamp('created_at')->change();
            $table->timestamp('updated_at')->change();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropUnique(['code']);
            $table->dropForeign(['status_id']);

            $table->dropColumn([
                'type',
                'vendor_code',
                'allow_publish',
                'status_id',
                'is_adult',
                'status_comment',
            ]);

            $table->dropIndex(['barcode']);
            $table->dropIndex(['category_id']);
            $table->dropIndex(['external_id']);

            $table->timestamp('created_at')->change();
            $table->timestamp('updated_at')->change();
            $table->string('external_id')->unique()->nullable(false)->change();

            $table->tinyInteger('archive')->unsigned()->default(0);
            $table->text('archive_comment')->nullable();
            $table->dateTime('archive_date')->nullable();
            $table->boolean('is_new')->default(false);

            $table->text('ingredients')->nullable();
            $table->integer('calories')->nullable();
            $table->decimal('proteins')->nullable();
            $table->decimal('fats')->nullable();
            $table->decimal('carbohydrates')->nullable();
            $table->text('storage_area')->nullable();
            $table->string('storage_address')->nullable();
            $table->text('shelf_life')->nullable();
            $table->text('additional_info')->nullable();
            $table->integer('vat_rate')->nullable();
            $table->unsignedBigInteger('variant_group_id')->nullable();

            $table->decimal('weight', 18, 4)->default(0)->change();
            $table->decimal('weight_gross', 18, 4)->default(0)->change();
            $table->decimal('width', 18, 4)->default(0)->change();
            $table->decimal('height', 18, 4)->default(0)->change();
            $table->decimal('length', 18, 4)->default(0)->change();

            $table->foreignId('manufacturer_id')->nullable()->constrained('manufacturers');
            $table->foreignId('country_id')->nullable()->constrained('countries');
            $table->foreignId('product_type_id')->nullable()->constrained('product_types');
        });
    }

    private function restoreClassifiers(): void
    {
        Schema::create('manufacturers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');

            $table->timestamps();
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');

            $table->timestamps();
        });

        Schema::create('product_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');

            $table->timestamps();
        });

        $this->addProductTypes();
    }

    private function addProductTypes(): void
    {
        DB::table('product_types')->insert(['code' => 'per_item', 'name' => 'штучный']);
        DB::table('product_types')->insert(['code' => 'per_kilo', 'name' => 'весовой']);
        DB::table('product_types')->insert(['code' => 'per_package', 'name' => 'фасованный']);
    }
}
