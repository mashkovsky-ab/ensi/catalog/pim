<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('gas')->default(false);
            $table->text('need_special_case')->nullable();
            $table->text('need_special_store')->nullable();
            $table->boolean('fragile')->default(false);
            $table->integer('days_to_return')->nullable();
        });

        Schema::create('product_constraints', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('product_id')->unsigned();
            $table->integer('type');
            $table->string('value');
            $table->boolean('deny')->default(false);

            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_constraints');

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('gas');
            $table->dropColumn('need_special_case');
            $table->dropColumn('need_special_store');
            $table->dropColumn('fragile');
            $table->dropColumn('days_to_return');
        });
    }
}
