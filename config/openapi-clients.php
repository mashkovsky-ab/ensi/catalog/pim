<?php

return [
    'catalog' => [
        'offers' => [
            'base_uri' => env('OFFERS_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
