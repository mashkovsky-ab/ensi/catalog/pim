# Ensi PIM

## Резюме

Название: PIM  
Домен: Catalog  
Назначение: Управление товарами  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

Регламент работы над задачами тоже находится в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/477528081)

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Зависимости

| Название | Описание  | Переменные окружения |
|---|---|---|
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| Elasticsearch (ES) | Нереляционное хранилище данных по товарам. | ELASTICSEARCH_HOST |
| Kafka | Брокер сообщений. <br/>Producer в данном сервисе не используется.<br/> Consumer слушает следующие топики:<br/> - `<контур>.cms.fact.product-group-products.1` | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |
| **Сервисы Ensi** | **Сервисы Ensi, с которыми данный сервис коммуницирует** |
| Catalog | Ensi Offers | OFFERS_SERVICE_HOST |
| Units | Ensi Business Units | BU_SERVICE_HOST |
| Marketing | Ensi Marketing | MARKETING_SERVICE_HOST |
| CMS | Ensi Cms | CMS_SERVICE_HOST |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/catalog/job/pim/  
URL: https://pim-master-dev.ensi.tech/docs/swagger

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
