<?php

use App\Domain\Categories\Models\PropertyType;
use App\Domain\Classifiers\Enums\MetricsCategory;
use App\Domain\Classifiers\Enums\ProductType;

return [
    // Типы атрибутов
    PropertyType::class => [
        PropertyType::STRING => 'Строка',
        PropertyType::BOOLEAN => 'Флажок',
        PropertyType::INTEGER => 'Целое число',
        PropertyType::DOUBLE => 'Вещественное число',
        PropertyType::DATETIME => 'Дата, время',
        PropertyType::IMAGE => 'Изображение',
        PropertyType::COLOR => 'Цвет',
    ],

    ProductType::class => [
        ProductType::PIECE->value => 'Штучный',
        ProductType::WEIGHT->value => 'Весовой',
        ProductType::PACKED->value => 'Фасованный',
    ],

    MetricsCategory::class => [
        MetricsCategory::MASTER->value => 'Мастер-данные',
        MetricsCategory::ATTRIBUTES->value => 'Атрибуты',
        MetricsCategory::CONTENT->value => 'Контент и медиа',
    ],
];
